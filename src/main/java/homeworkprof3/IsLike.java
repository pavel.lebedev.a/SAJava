package homeworkprof3;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)

public @interface IsLike {
    boolean check() default true;
}
