package homeworkprof3;

/*
Создать аннотацию @IsLike, применимую к классу во время выполнения
программы. Аннотация может хранить boolean значение.
 */

import java.lang.annotation.Annotation;

public class Task1 {
    public static void main(String[] args) {
        IsLikeTest isLikeTest = new IsLikeTest();
        Annotation annotation = isLikeTest.getClass().getAnnotation(IsLike.class);
        IsLike isLike = (IsLike)annotation;
        System.out.println(isLike.check());
    }
}
