package homeworkprof3;

/*
Написать метод, который рефлексивно проверит наличие аннотации @IsLike на
любом переданном классе и выведет значение, хранящееся в аннотации, на
экран.
 */

import java.lang.annotation.Annotation;

public class Task2 {
    public static void main(String[] args) {
        myMethod(String.class);
        myMethod(IsLikeTest.class);
    }

    public static void myMethod (Class c) {
        try {
            Annotation annotation = c.getAnnotation(IsLike.class);
            IsLike isLike = (IsLike)annotation;
            System.out.println(isLike.check());
        } catch (Exception e) {
            System.out.println("Аннотация @IsLike осутсвует");
        }
    }
}
