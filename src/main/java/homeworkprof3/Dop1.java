package homeworkprof3;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/*
Дана строка, состоящая из символов “(“ и “)”
Необходимо написать метод, принимающий эту строку и выводящий результат,
является ли она правильной скобочной последовательностью или нет.
Строка является правильной скобочной последовательностью, если:
● Пустая строка является правильной скобочной последовательностью.
● Пусть S — правильная скобочная последовательность, тогда (S) есть
правильная скобочная последовательность.
● Пусть S1, S2 — правильные скобочные последовательности, тогда S1S2
есть правильная скобочная последовательность.
 */
public class Dop1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        System.out.println(bracketValidation(s));
    }
    public static boolean bracketValidation (String s) {
        Queue<Character> stack = new LinkedList<>();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ')' && stack.isEmpty()) { // если закрывающая и стек пуст
                return false;
            } else if (s.charAt(i) == '(') { // если открывающая, добавляем в стек
                stack.add(s.charAt(i));
            } else if (s.charAt(i) == ')') { // если закрывающая, удаляем из стека
                stack.remove();
            }
        }
        return stack.isEmpty(); // в конце стек должен быть пуст
    }
}
