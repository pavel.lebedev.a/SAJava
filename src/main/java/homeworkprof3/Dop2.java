package homeworkprof3;
/*
Дана строка, состоящая из символов “(“, “)”, “{”, “}”, “[“, “]”
Необходимо написать метод, принимающий эту строку и выводящий результат,
является ли она правильной скобочной последовательностью или нет.
Условия для правильной скобочной последовательности те, же что и в задаче 1,
но дополнительно:
● Открывающие скобки должны быть закрыты однотипными
закрывающими скобками.
● Каждой закрывающей скобке соответствует открывающая скобка того же
типа.
 */

import java.util.*;

public class Dop2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        System.out.println(bracketsValidation(s));
    }

    private static boolean bracketsValidation(String s) {
        Map<Character, Character> map = new HashMap<>();
        map.put(')', '(');
        map.put('}', '{');
        map.put(']', '[');
        Deque<Character> stack = new LinkedList<>();
        for (char c : s.toCharArray()) {
            if (map.containsValue(c)) { // если открывающая, добавляем в стек
                stack.push(c);
            } else if (map.containsKey(c)) {
                if (stack.isEmpty() || stack.pop() != map.get(c)) { // если закрывающая, стек пуст или она другого типа
                    return false;
                }
            }
        }
        return stack.isEmpty(); // в конце стек должен быть пуст
    }
}
