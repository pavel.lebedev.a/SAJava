package homeworkprof3;

import java.util.Arrays;

/*
Написать метод, который с помощью рефлексии получит все интерфейсы
класса, включая интерфейсы от классов-родителей и интерфейсов-родителей.
 */
public class Task4 {
    public static void main(String[] args) {
        System.out.println("1\n");
        getAllInterfaces(String.class);
        System.out.println("\n2\n");
        getAllInterfaces(Integer.class);

    }
    public static void getAllInterfaces (Class c){
        try {
            System.out.println("Class");
            String fullClassName = c.getName();
            System.out.println(fullClassName);

            System.out.println("Class Interfaces");
            Class[] classInterfaces = c.getInterfaces();
            for (Class classInterface : classInterfaces) {
                System.out.println(classInterface.getName());
            }

            System.out.println("Super Class");
            Class superclass = c.getSuperclass();
            System.out.println(superclass);

            System.out.println("Super Class Interfaces");
            Class[] superClassInterfaces = superclass.getInterfaces();
            for (Class superClassInterface : superClassInterfaces) {
                System.out.println(superClassInterface.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
