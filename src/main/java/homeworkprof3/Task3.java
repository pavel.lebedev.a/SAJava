package homeworkprof3;

import java.lang.reflect.*;
import java.util.Arrays;

/*
Есть класс APrinter:
Задача: с помощью рефлексии вызвать метод print() и обработать все
возможные ошибки (в качестве аргумента передавать любое подходящее
число). При “ловле” исключений выводить на экран краткое описание ошибки.
 */
public class Task3 {
    public static void main(String[] args) {
        APrinter aPrinter = new APrinter();
        try {
            Class clazz = Class.forName(APrinter.class.getName());
            System.out.println("Имя класса: " + clazz);
            Method[] methods = clazz.getDeclaredMethods();
            Class<?>[] params = new Class[0];
            for (Method method : methods) {
                System.out.println("Method name : " + method.getName());
                params = method.getParameterTypes();
                System.out.print("Parameters : ");
                for (Class<?> param : params)
                    System.out.print(" " + param.getName());
                System.out.println();
            }
            Method p = clazz.getMethod("print", params[0]);
            for (int i = 0; i < 100; i++) {
                int n = (int) (Math.random() * 10);
                p.invoke(aPrinter, n);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
