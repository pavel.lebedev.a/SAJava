package homeworkprof1;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormValidator {
    private static final String SUCCESS = "Проверка пройдена успешно";

    // public void checkName(String str) — длина имени должна быть от 2 до 20 символов, первая буква заглавная.
    public static void checkName(String str) {
        try {
            if (Character.isUpperCase(str.charAt(0))) {
                if (str.length() > 1 && str.length() < 21) {
                    System.out.println("checkName: " + SUCCESS);
                } else throw new Exception();
            } else throw new Exception();
        } catch (Exception e) {
            throw new RuntimeException("Первая буква должна быть заглавной и длинна имени должна быть от 2 до 20", e);
        }
    }

    // public void checkBirthdate(String str) — дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты.
    public static void checkBirthdate(String str) {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date;
        Date currentDate = new Date();
        try {
            date = dateFormat.parse(str);
            if (date.after(dateFormat.parse("01.01.1900")) && date.before(currentDate)) {
                System.out.println("checkBirthdate: " + SUCCESS);
            } else throw new Exception();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException("Дата рождения должна быть не раньше 01.01.1900 и не позже текущей даты", e);
        }
    }

    // public void checkGender(String str) — пол должен корректно матчится в enum Gender, хранящий Male и Female значения.
    public static void checkGender(String str) {
        try {
            if (str.equals("Male") || str.equals("Female")) {
                System.out.println("checkGender: " + SUCCESS);
            } else throw new Exception();
        } catch (Exception e) {
            throw new RuntimeException("Пол должен быть Male или Female", e);
        }
    }

    // public void checkHeight(String str) — рост должен быть положительным числом и корректно конвертироваться в double.
    public static void checkHeight(String str) {
        double d = Double.parseDouble(str);
        try {
            if (d > 0) {
                System.out.println(d);
                System.out.println("checkHeight: " + SUCCESS);
            } else throw new Exception();
        } catch (Exception e) {
            throw new RuntimeException("Рост должен быть положительным числом", e);
        }
    }

}
