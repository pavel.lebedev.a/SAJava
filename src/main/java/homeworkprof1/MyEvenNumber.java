package homeworkprof1;

public class MyEvenNumber {
    int n;

    public MyEvenNumber(int n) {
        try {
            if (n % 2 == 0) {
                this.n = n;
            } else throw new RuntimeException("Только чётные числа");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }
}
