package homeworkprof1;

import java.util.Scanner;

/*
На вход подается число n, массив целых чисел отсортированных по
возрастанию длины n и число p. Необходимо найти индекс элемента массива
равного p. Все числа в массиве уникальны. Если искомый элемент не найден,
вывести -1.
Решить задачу за логарифмическую сложность.
Пример:
Входные данные Выходные данные
5  /  -42 -12 3 5 8  /  5  //  3
2  /  17 19  /  20  // -1
 */
public class Dop2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        int p = scanner.nextInt();
        System.out.println(binarySearch(array, p));
    }
    private static int binarySearch(int[] array, int n) {
        int index = -1;
        int low = 0;
        int high = array.length - 1;
        while (low <= high) {
            int mid = (low + high) / 2;
            if (array[mid] < n) {
                low = mid + 1;
            } else if (array[mid] > n) {
                high = mid - 1;
            } else if (array[mid] == n) {
                index = mid;
                break;
            }
        }
        return index;
    }
}
