package homeworkprof1;

public class MyUncheckedException extends RuntimeException {
    @Override
    public void printStackTrace() {
        super.printStackTrace();
    }

    public MyUncheckedException (String message) {
        super(message);
    }
}
