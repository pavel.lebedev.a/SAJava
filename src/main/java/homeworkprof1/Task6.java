package homeworkprof1;
/*
Фронт со своей стороны не сделал обработку входных данных анкеты! Петя
очень зол и ему придется написать свои проверки, а также кидать исключения,
если проверка провалилась. Помогите Пете написать класс FormValidator со
статическими методами проверки. На вход всем методам подается String str.

a. public void checkName(String str) — длина имени должна быть от 2 до 20
символов, первая буква заглавная.

b. public void checkBirthdate(String str) — дата рождения должна быть не
раньше 01.01.1900 и не позже текущей даты.

c. public void checkGender(String str) — пол должен корректно матчится в
enum Gender, хранящий Male и Female значения.

d. public void checkHeight(String str) — рост должен быть положительным
числом и корректно конвертироваться в double.
 */
public class Task6 {
    public static void main(String[] args) {
        String Name1 = "QazwsXedcRfvtgbyhnujmIk,";
        String Name2 = "Qazwsx";
        String Gender1 = "Dog";
        String Gender2 = "Male";
        String Height1 = "-24.5";
        String Height2 = "176.5";
        String Birthdate1 = "01.01.1800";
        String Birthdate2 = "05.05.2123";
        String Birthdate3 = "31.12.1980";

//        FormValidator.checkName(Name1);
//        FormValidator.checkName(Name2);
//        FormValidator.checkGender(Gender1);
//        FormValidator.checkGender(Gender2);
//        FormValidator.checkHeight(Height1);
//        FormValidator.checkHeight(Height2);
//        FormValidator.checkBirthdate(Birthdate1);
//        FormValidator.checkBirthdate(Birthdate2);
//        FormValidator.checkBirthdate(Birthdate3);


    }
}
