package homeworkprof1;

/*
Создать класс MyEvenNumber, который хранит четное число int n. Используя
исключения, запретить создание инстанса MyPrimeNumber с нечетным числом.
 */

public class Task4 {
    public static void main(String[] args) {
        MyEvenNumber myEvenNumber1 = new MyEvenNumber(2);
        System.out.println(myEvenNumber1.getN());
        MyEvenNumber myEvenNumber2 = new MyEvenNumber(3);
        System.out.println(myEvenNumber2.getN());
    }
}
