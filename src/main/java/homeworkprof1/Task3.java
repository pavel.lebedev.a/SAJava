package homeworkprof1;

/*
Реализовать метод, открывающий файл ./input.txt и сохраняющий в файл
./output.txt текст из input, где каждый латинский строчный символ заменен на
соответствующий заглавный. Обязательно использование try с ресурсами.
 */

import java.io.*;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        String path = "C:\\Users\\Varga\\IdeaProjects\\SAJava\\src\\main\\java\\homeWorkProf1";
        String absolutePathInput = path + File.separator + "input.txt";
        String absolutePathOutput = path + File.separator + "output.txt";

        printFile(absolutePathInput);
        inputToUpperOutput(absolutePathInput, absolutePathOutput);
        printFile(absolutePathOutput);
    }

    /**
     * Запись из файла в файл Латинских символов в верхнем регистре
     * @param absolutePathInput Путь к файлу источнику
     * @param absolutePathOutput Путь к целевому файлу
     */
    static void inputToUpperOutput(String absolutePathInput, String absolutePathOutput) {
        try (Scanner scanner = new Scanner(new File(absolutePathInput))) {
            while (scanner.hasNextLine()) {
                char[] c = scanner.nextLine().toCharArray();
                for (int i = 0; i < c.length; i++) {
                    if (c[i] >= 'a' && c[i] <= 'z') {
                        c[i] = Character.toUpperCase(c[i]);
                    }
                }
                String s = "";
                for (int i = 0; i < c.length; i++) {
                    s += Character.toString(c[i]);
                }
                writeToFile(s, absolutePathOutput);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

//    /**
//     * Запись из файла в файл большими буквами
//     * @param absolutePathInput Путь к файлу источнику
//     * @param absolutePathOutput Путь к целевому файлу
//     */
//    static void inputToUpperOutput(String absolutePathInput, String absolutePathOutput) {
//        try (Scanner scanner = new Scanner(new File(absolutePathInput))) {
//            String s = new String();
//            while (scanner.hasNextLine()) {
//                s = scanner.nextLine().toUpperCase();
//                writeToFile(s, absolutePathOutput);
//            }
//        } catch (FileNotFoundException e) {
//            throw new RuntimeException(e);
//        }
//    }

    /**
     * Запись строки в файл
     * @param s Строка
     * @param absolutePathOutput Путь к файлу
     */
    static void writeToFile(String s, String absolutePathOutput) {
        try (FileWriter fileWriter = new FileWriter(absolutePathOutput, true)) {
            fileWriter.write(s);
            fileWriter.write("\n");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Построчная печать в консоль из файла
     * @param absolutePath Путь к файлу
     */
    static void printFile(String absolutePath) {
        try (Scanner scanner = new Scanner(new File(absolutePath));) {
            while (scanner.hasNextLine()) {
                System.out.println(scanner.nextLine());
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
