package homeworkprof1;

// Создать собственное исключение MyCheckedException, являющееся проверяемым

public class Task1 {
    public static void main(String[] args) throws MyCheckedException {
        int n = 0;
        try {
            if (n == 0) {
                throw new MyCheckedException();
            }
        } catch (MyCheckedException e) {
            throw new MyCheckedException("Неправильное число");
        }
    }
}
