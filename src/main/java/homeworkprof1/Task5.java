package homeworkprof1;

// Найти и исправить ошибки в следующем коде (сдать исправленный вариант)

import java.util.Scanner; // импорт сканера

public class Task5 {
    public static void main(String[] args) throws Exception { // проброс исключения
        int n = inputN();
        System.out.println("Успешный ввод!");
    }
    private static int inputN() throws Exception { // проброс исключения
        System.out.println("Введите число n, 0 < n < 100");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n < 100 && n > 0) {
            return n;
        }  //  /\ перемена местами строк \/
        throw new Exception("Неверный ввод");
    }
}
