package homeworkprof1;

import java.util.Scanner;

/*
На вход подается число n и массив целых чисел длины n.
Вывести два максимальных числа в этой последовательности.
Пояснение: Вторым максимальным числом считается тот, который окажется
максимальным после вычеркивания первого максимума.
Пример:
Входные данные Выходные данные
5
1 3 5 4 5  //  5 5
3
3 2 1  //  3 2
 */
public class Dop1 {
    public static void main(String[] args) {
        int n;
        int[] array;
        try (Scanner scanner = new Scanner(System.in)){
            n = scanner.nextInt();
            array = new int[n];
            for (int i = 0; i < array.length; i++) {
                array[i] = scanner.nextInt();
            }
            for (int j = 0; j < 2; j++) {
                for (int i = array.length - 1; i > 0; i--) {
                    int tmp = 0;
                    if (array[i] > array[i - 1]) {
                        tmp = array[i];
                        array[i] = array[i - 1];
                        array[i - 1] = tmp;
                    }
                }
            }
            System.out.println(array[0] + " " + array[1]);
        } catch (Exception e) {
            throw new RuntimeException("Только целые числа", e);
        }
    }
}
