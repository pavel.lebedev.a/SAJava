package homework3_1;

/*
Необходимо реализовать класс Cat.
У класса должны быть реализованы следующие приватные методы:
● sleep() — выводит на экран “Sleep”
● meow() — выводит на экран “Meow”
● eat() — выводит на экран “Eat”
И публичный метод:
status() — вызывает один из приватных методов случайным образом.
 */

public class Task1 {
    public static void main(String[] args) {
        for (int i = 0; i < 10 ; i++) { // Test
            status();
        }
    }

    public static void status() {
        int n = (int) (Math.random() * 3 + 1) - 1; // Генератор принятия решений.
        if (n == 0) {
            meow();
        } else if (n == 1) {
            eat();
        } else {
            sleep();
        }
    }

    private static void sleep() {
        System.out.println("Sleep");
    }

    private static void meow() {
        System.out.println("Meow");
    }

    private static void eat() {
        System.out.println("Eat");
    }
}

