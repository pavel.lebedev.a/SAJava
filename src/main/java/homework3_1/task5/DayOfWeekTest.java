package homework3_1.task5;
/*
Необходимо реализовать класс DayOfWeek для хранения порядкового номера
дня недели (byte) и названия дня недели (String).
Затем в отдельном классе в методе main создать массив объектов DayOfWeek
длины 7. Заполнить его соответствующими значениями (от 1 Monday до 7
Sunday) и вывести значения массива объектов DayOfWeek на экран.
Пример вывода:
1 Monday
2 Tuesday
…7 Sunday
 */

public class DayOfWeekTest {
    public static void main(String[] args) {
        DayOfWeek[] dayOfWeeks = new DayOfWeek[7];
        dayOfWeeks[0] = new DayOfWeek((byte) 1, "Monday");
        dayOfWeeks[1] = new DayOfWeek((byte) 2, "Tuesday");
        dayOfWeeks[2] = new DayOfWeek((byte) 3, "Wednesday");
        dayOfWeeks[3] = new DayOfWeek((byte) 4, "Thursday");
        dayOfWeeks[4] = new DayOfWeek((byte) 5, "Friday");
        dayOfWeeks[5] = new DayOfWeek((byte) 6, "Saturday");
        dayOfWeeks[6] = new DayOfWeek((byte) 7, "Sunday");
        for (DayOfWeek dayOfWeek : dayOfWeeks) {
            System.out.println(dayOfWeek.getDayNum() + " " + dayOfWeek.getDayName());
        }
    }
}
