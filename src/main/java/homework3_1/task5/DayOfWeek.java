package homework3_1.task5;

public class DayOfWeek {
    private byte dayNum;
    private String dayName;

    protected DayOfWeek() {}

    protected DayOfWeek(byte dayNum, String dayName) {
        this.dayNum = dayNum;
        this.dayName = dayName;
    }

    protected byte getDayNum() {
        return dayNum;
    }

    protected String getDayName() {
        return dayName;
    }
}
