package homework3_1.task2_task3;

import java.util.Arrays;
/* Код работает, но есть 2 непонятных ошибки от IDEA
"Cannot access HomeWork3_1.Task2_Task3.Student:40"
"Cannot access HomeWork3_1.Task2_Task3.Student:44"
на вызове StudentService
 */
public class StudentTest {
    public static void main(String[] args) {
        testTask2(); // test Task2
        testTask3(); // test Task3
    }
    private static void testTask2() {
        // Task2, Test 1, добавить студента и оценки, вывести результат
        System.out.println("Task2, Test 1, добавить студента и оценки, вывести результат");
        Student student = new Student(studentGenerationName(), studentGenerationSurName(), studentGenerationScore(10));
        studentPrint(student);
        // Task2, Test 2, изменить оценку, вывести результат
        System.out.println("Task2, Test 2, добавить оценку 5, вывести результат");
        student.setAddScore(5);
        studentPrint(student);
        System.out.println("Task2, Test 3, добавить оценку 5 к 4-ём оценкам, вывести результат");
        Student student1 = new Student(studentGenerationName(), studentGenerationSurName(), studentGenerationScore(4));
        System.out.println("Task2, Test 3, до добавления");
        studentPrint(student1);
        student1.setAddScore(5);
        System.out.println("Task2, Test 3, после добавления");
        studentPrint(student1);
        // Task2, Test 3, создать нового студента и оценки, вывести результат
        System.out.println("Task2, Test 4, создать нового студента и оценки, вывести результат");
        student = new Student(studentGenerationName(), studentGenerationSurName(), studentGenerationScore(6));
        studentPrint(student);
    }

    private static void testTask3() {
        // Task3, Test 1, Создание и проверка массива Студентов
        System.out.println("Task3, Test 1, Создание и проверка массива Студентов");
        Student[] studentArray = new Student[5];
        for (int i = 0; i < studentArray.length; i++) { // Заполнение массива
            studentArray[i] = new Student(studentGenerationName(), studentGenerationSurName(), studentGenerationScore(10));
        }
        for (Student value : studentArray) { // Вывод массива
            studentPrint(value);
        }
        // Task3, Test 2, Лучший студент
        System.out.println("Task3, Test 2, Лучший студент");
        Student best = StudentService.getBestStudent(studentArray);
        studentPrint(best);
        // Task3, Test 3, Сортировка по фамилии
        System.out.println("Task3, Test 3, Сортировка по фамилии");
        StudentService.sortBySurname(studentArray);
        for (Student student : studentArray) {
            studentPrint(student);
        }

    }

    /** Вывод информации о студенте */
    private static void studentPrint(Student student) {
        System.out.print(student.getName() + " ");
        System.out.print(student.getSurname() + " ");
        System.out.print(Arrays.toString(student.getGrades()) + " ");
        System.out.print(student.getGradeScoreAverage());
        System.out.println("");
    }

    /** Создание массива оценок, длиной n */
    private static int[] studentGenerationScore(int n) { // Генератор массивов оценок
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 11 + 1) - 1;
        }
        return array;
    }

    /** Генератор имени */
    private static String studentGenerationName() { // Генератор имён
        String[] nameArray = {"Иван", "Егор", "Павел", "Михаил", "Сергей", "Виктор", "Андрей", "Григорий", "Константин", "Олег"};
        return nameArray[(int) (Math.random() * 10 + 1) - 1];
    }

    /** Генератор фамилии */
    private static String studentGenerationSurName() { // Генератор Фамилий
        String[] surNameArray = {"Иванов", "Морозов", "Галкин", "Волков", "Зайцев", "Белкин", "Лисичкин", "Енотов", "Железнов", "Сорокин"};
        return surNameArray[(int) (Math.random() * 10 + 1) - 1];
    }
}
