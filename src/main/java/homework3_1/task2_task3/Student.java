package homework3_1.task2_task3;

/*
Task 2
Необходимо реализовать класс Student.
У класса должны быть следующие приватные поля:
● String name — имя студента
● String surname — фамилия студента
● int[] grades — последние 10 оценок студента. Их может быть меньше, но не может быть больше 10.
И следующие публичные методы:
● геттер/сеттер для name, surname, grades.
● метод, добавляющий новую оценку в grades. Самая первая оценка должна быть удалена,
новая должна сохраниться в конце массива (т.е. массив должен сдвинуться на 1 влево).
● метод, возвращающий средний балл студента (рассчитывается как
среднее арифметическое от всех оценок в массиве grades)
 */
public class Student {
    private String name;
    private String surname;
    private int[] grades;

    public Student() {}

    protected Student(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    protected Student(String name, String surname, int[] grades) {
        this.name = name;
        this.surname = surname;
        this.grades = grades;
    }

    // getter-методы для возврата значения
    protected String getName() {
        return name;
    }

    protected String getSurname() {
        return surname;
    }

    protected int[] getGrades() {
        return grades;
    }

    protected double getGradeScoreAverage() {
        return gradeScoreAverage(grades);
    }

    // setter-методы для присваивания нового значения
    protected void setName(String name) {
        this.name = name;
    }

    protected void setSurname(String surname) {
        this.surname = surname;
    }

    protected void setGrades(int[] grades) {
        this.grades = grades;
    }

    protected void setAddScore(int score) {
        if (grades.length < 10) {
            int[] tmpGrades = new int[grades.length + 1];
            for (int i = 0; i < grades.length; i++) {
                tmpGrades[i] = grades[i];
            }
            tmpGrades[tmpGrades.length - 1] = score;
            grades = tmpGrades;
        } else shiftAndNewNumArray(grades, score);
    }

    /**
     * Циклически сдвигает Числа в массиве на 1 шаг в лево
     * и присваивает новое значение в конец массива
     */
    private static void shiftAndNewNumArray(int[] array, int n) {
        for (int i = 0; i < array.length - 1; i++) {
            array[i] = array[i + 1];
        }
        array[array.length - 1] = n;
    }

    /**
     * Метод вычисляет среднеарифметическое всех чисел массива.
     * Формула: сумма всех чисел массива / на длину массива.
     */
    private static double gradeScoreAverage(int[] array) {
        double sum = 0.0;
        int i;
        for (i = 0; i < array.length; i++) {
            sum += array[i];
        }
        sum = sum / i * 10;
        sum = (int)sum;
        return sum = (double) sum / 10;
    }
}
