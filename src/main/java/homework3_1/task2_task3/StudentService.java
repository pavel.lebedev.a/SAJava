package homework3_1.task2_task3;

/*
Task 3
Необходимо реализовать класс StudentService.
У класса должны быть реализованы следующие публичные методы:
● bestStudent() — принимает массив студентов (класс Student из предыдущего задания),
возвращает лучшего студента (т.е. который имеет самый высокий средний балл). Если таких несколько — вывести любого.
● sortBySurname() — принимает массив студентов (класс Student из предыдущего задания) и сортирует его по фамилии.
 */
public class StudentService {
    protected static Student bestStudent = new Student();

    // Конструктор
    protected StudentService() {
    }

    // Getter methods
    protected static Student getBestStudent(Student[] students) {
        return searchBestStudent(students);
    }

    /**
     * Находит лучшего студента в массиве
     */
    private static Student searchBestStudent(Student[] students) {
        double n = 0.0;
        for (Student student : students) {
            if (student.getGradeScoreAverage() > n) {
                n = student.getGradeScoreAverage();
                bestStudent = student;
            }
        }
        return bestStudent;
    }

    /**
     * Сортирует студентов по фамилии и имени
     */
    protected static void sortBySurname(Student[] students) {
        for (int i = students.length - 1; i >= 0; i--) { // Проверка и сортировка по Фамилии
            for (int j = 0; j < i; j++) {
                if (students[i].getSurname().compareTo(students[j].getSurname()) < 0) {
                    transposition(students, i, j);
                } else if (students[i].getSurname().compareTo(students[j].getSurname()) == 0) { // Если фамилии совпадают
                    if (students[i].getName().compareTo(students[j].getName()) < 0) { // то по имени
                        transposition(students, i, j);
                    }
                }
            }
        }
    }

    /**
     * Перестановка студентов
     */
    private static void transposition(Student[] sortStudents, int i, int j) {
        Student tmp = new Student();
        tmp = sortStudents[i];
        sortStudents[i] = sortStudents[j];
        sortStudents[j] = tmp;
    }
}
