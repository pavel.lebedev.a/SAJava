package homework3_1;
/*
 Реализовать класс “банкомат” Atm.
Класс должен:
● Содержать конструктор, позволяющий задать курс валют перевода
долларов в рубли и курс валют перевода рублей в доллары (можно
выбрать и задать любые положительные значения)
● Содержать два публичных метода, которые позволяют переводить
переданную сумму рублей в доллары и долларов в рубли
● Хранить приватную переменную счетчик — количество созданных
инстансов класса Atm и публичный метод, возвращающий этот счетчик
(подсказка: реализуется через static)
 */
public class Task8 {
public static void main(String[]args) {
    Atm atm1 = new Atm(60.14, 58.65);
    double exchange1 = Atm.dollarsToRublesExchange(atm1, 100);
    System.out.println("счетчик инстансов класса Atm = " + Atm.getCounterAtm());
    Atm atm2 = new Atm(64.58, 60.34);
    double exchange2 = Atm.rublesToDollarsExchange(atm2, 12345);
    System.out.println("счетчик инстансов класса Atm = " + Atm.getCounterAtm());
    }
}

class Atm {
    private static double dollarsToRubles;
    private static double rublesToDollars;
    private static int counterAtm = 0;

    // Constructors
    public Atm(double dollarsToRubles, double rublesToDollars) {
        if (dollarsToRubles > 0 && rublesToDollars > 0) {
            this.dollarsToRubles = dollarsToRubles;
            this.rublesToDollars = rublesToDollars;
            counterAtm++;
        } else System.out.println("Курс должен быть больше нуля");
    }

    // Getters
    public static int getCounterAtm() {
        return counterAtm;
    }

    public static double getDollarsToRubles() {
        return dollarsToRubles;
    }

    public static double getRublesToDollars() {
        return rublesToDollars;
    }
    // Methods
    static double dollarsToRublesExchange(Atm atm, int currency) {
        if (currency <= 0) {
            System.out.println("Валюты должно быть больше 0");
            return 0;
        } else {
            double out;
            System.out.println("Ваша валюта: " + currency + " $. Курс обмена: " + getDollarsToRubles());
            out = Math.floor(((double)currency * getDollarsToRubles()) * 100) / 100;
            System.out.println("при конвертации вы получили: " + out + " ₽");
            return out;
        }
    }
    static double rublesToDollarsExchange (Atm atm, int currency) {
        if (currency <= 0) {
            System.out.println("Валюты должно быть больше 0");
            return 0;
        } else {
            double out;
            System.out.println("Ваша валюта: " + currency + " ₽. Курс обмена: " + getRublesToDollars());
            out = Math.floor(((double)currency / getRublesToDollars()) * 100) / 100;
            System.out.println("при конвертации вы получили: " + out + " $");
            return out;
        }
    }
}
