package homework3_1;

/*
Реализовать класс TriangleChecker, статический метод которого принимает три
длины сторон треугольника и возвращает true, если возможно составить из них
треугольник, иначе false. Входные длины сторон треугольника — числа типа double.
Придумать и написать в методе main несколько тестов для проверки
работоспособности класса (минимум один тест на результат true и один на
результат false)
 */
public class Task7 {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            double a, b, c;
            a = Math.floor(Math.random() * 100) / 10;
            b = Math.floor(Math.random() * 100) / 10;
            c = Math.floor(Math.random() * 100) / 10;
            System.out.println("При a = " + a + ", b = " + b + ", c = " + c + ". Triangle check = " + TriangleChecker.checker(a, b ,c));
        }
        // Правило сторон треугольника утверждает, что сумма длин любых двух сторон треугольника должна быть больше длины третьей стороны
        System.out.println("При a = " + 1 + ", b = " + 1 + ", c = " + 3 + ". По правилу false. Triangle check = " + TriangleChecker.checker(1, 1 ,3));
        System.out.println("При a = " + 1 + ", b = " + 1 + ", c = " + 1 + ". По правилу true. Triangle check = " + TriangleChecker.checker(1, 1 ,1));
    }
}

class TriangleChecker {
    // Правило сторон треугольника утверждает, что сумма длин любых двух сторон треугольника должна быть больше длины третьей стороны
    static boolean checker(double a, double b, double c) {
        return (a + b) > c && (a + c) > b && (b + c) > a;
    }
}