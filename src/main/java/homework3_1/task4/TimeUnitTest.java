package homework3_1.task4;


public class TimeUnitTest {
    public static void main(String[] args) throws TimeUnitException {
        System.out.println("Test 1");
        TimeUnit test1 = new TimeUnit(23, 55, 45);
        TimeUnit test2 = new TimeUnit(2, 12);
        TimeUnit test3 = new TimeUnit(12);
        test1.getTime(24);
        test1.getTime(12);
        test1.getTime(24);
        test1.getTime(12);
        test1.getTime(24);
        test1.getTime(12);
        System.out.println("Test 2");
        test3.addTime(5, 5, 5);
        test3.getTime(24);
        test3.getTime(12);
        System.out.println("Test 3");
        test3.getTime(24);
        System.out.println("\nВызов ошибки");
        test3.addTime(15, 15, 15);
    }
}
