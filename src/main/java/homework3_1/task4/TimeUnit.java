package homework3_1.task4;

/*
Необходимо реализовать класс TimeUnit с функционалом, описанным ниже (необходимые поля продумать самостоятельно).
Обязательно должны быть реализованы валидации на входные параметры.
Конструкторы:
● Возможность создать TimeUnit, задав часы, минуты и секунды.
● Возможность создать TimeUnit, задав часы и минуты. Секунды тогда должны проставиться нулевыми.
● Возможность создать TimeUnit, задав часы. Минуты и секунды тогда должны проставиться нулевыми.
Публичные методы:
● Вывести на экран установленное в классе время в формате hh:mm:ss
● Вывести на экран установленное в классе время в 12-часовом формате (используя hh:mm:ss am/pm)
● Метод, который прибавляет переданное время к установленному в TimeUnit (на вход передаются только часы, минуты и секунды).
 */

public class TimeUnit {
    private int hours;
    private int minutes;
    private int seconds;

    // Constructors
    public TimeUnit() {}

    public TimeUnit(int hours) throws TimeUnitException {
        this(hours, 0, 0);
    }

    protected TimeUnit(int hours, int minutes) throws TimeUnitException {
        this(hours, minutes, 0);
    }

    protected TimeUnit(int hours, int minutes, int seconds) throws TimeUnitException {
        if (validationHh(hours)) {
            this.hours = hours;
        }
        if (validationMmSs(minutes)) {
            this.minutes = minutes;
        }
        if (validationMmSs(seconds)) {
            this.seconds = seconds;
        }
    }

    // Setters
    protected void setHours(int hours) throws TimeUnitException {
        if (validationHh(hours)) {
            this.hours = hours;
        }
    }

    protected void setMinutes(int minutes) throws TimeUnitException {
        if (validationMmSs(minutes)) {
            this.minutes = minutes;
        }
    }

    protected void setSeconds(int seconds) throws TimeUnitException {
        if (validationMmSs(seconds)) {
            this.seconds = seconds;
        }
    }

    // Getters
    protected int getHours() {
        return hours;
    }

    protected int getMinutes() {
        return minutes;
    }

    protected int getSeconds() {
        return seconds;
    }

    /**
     * Выводит время в 12 или 24 часовом формате
     * @param format число 12 или 24
     */
    protected void getTime(int format) { // format 12/24 timeFormat false/true
        if (format == 24) {
            printTime(true);
        } else if (format == 12) {
            printTime(false);
        } else System.out.println("Некорректный формат времени");
    }

    // Methods
    private boolean validationHh (int n) throws TimeUnitException {
        if (n < 0 || n > 23) {
            throw new TimeUnitException();
        } else return true;
    }

    private boolean validationMmSs (int n) throws TimeUnitException {
        if (n < 0 || n > 59) {
            throw new TimeUnitException();
        } else return true;
    }

    /**
     * Вывести время format 12/24 timeFormat false/true
     */
    private void printTime(boolean timeFormat) {
        System.out.println("");
        if (timeFormat) { // format 24 timeFormat true
            normHours();
            normMinutesSeconds();
        } else { // format 12 timeFormat false
            if (getHours() == 12) {
                System.out.print("00:");
                if (getMinutes() > 10) {
                    System.out.print(getMinutes() + ":");
                } else hhMm(getMinutes());
                if (getSeconds() > 10) {
                    System.out.print(getSeconds());
                } else ss(getSeconds());
            } else if (getHours() < 12) {
                normHours();
                normMinutesSeconds();
            } else {
                if (getHours() - 12 < 10) {
                    System.out.print("0" + (getHours() - 12) + ":");
                    normMinutesSeconds();
                } else {
                    System.out.print(getHours() - 12 + ":");
                    normMinutesSeconds();
                }
            }
            amPm();
        }
    }

    /** print hours || minutes */
    private void hhMm(int n) {
        System.out.print("0" + n + ":");
    }

    /** print seconds */
    private void ss(int n) {
        System.out.print("0" + n);
    }

    /** print norm time hh */
    private void normHours() {
        if (getHours() > 10) {
            System.out.print(getHours() + ":");
        } else hhMm(getHours());
    }

    /** print norm time mmss */
    private void normMinutesSeconds() {
        if (getMinutes() > 10) {
            System.out.print(getMinutes() + ":");
        } else hhMm(getMinutes());
        if (getSeconds() > 10) {
            System.out.print(getSeconds());
        } else ss(getSeconds());
    }

    /** time format am/pm */
    private void amPm() {
        if (getHours() < 12) {
            System.out.println(" a.m.");
        } else System.out.println(" p.m.");
    }

    /** Добавить время */
    protected void addTime(int h, int m, int s) throws TimeUnitException {
        int n;
        n = getHours() + h;
        if (validationHh(n)) {
            setHours(n);
        }
        n = getMinutes() + m;
        if (validationMmSs(n)) {
            setMinutes(n);
        }
        n = getSeconds() + s;
        if (validationMmSs(n)) {
            setSeconds(n);
        }
    }
}
