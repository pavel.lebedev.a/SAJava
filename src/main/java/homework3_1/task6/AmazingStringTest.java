package homework3_1.task6;
/*
Необходимо реализовать класс AmazingString, который хранит внутри себя
строку как массив char и предоставляет следующий функционал:
Конструкторы:
● Создание AmazingString, принимая на вход массив char
● Создание AmazingString, принимая на вход String
Публичные методы (названия методов, входные и выходные параметры
продумать самостоятельно).

Все методы ниже нужно реализовать “руками”, т.е.
не прибегая к переводу массива char в String и без использования стандартных
методов класса String.

● Вернуть i-ый символ строки
● Вернуть длину строки
● Вывести строку на экран
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается массив char). Вернуть true, если найдена и false иначе
● Проверить, есть ли переданная подстрока в AmazingString (на вход
подается String). Вернуть true, если найдена и false иначе
● Удалить из строки AmazingString ведущие пробельные символы, если
они есть
● Развернуть строку (первый символ должен стать последним, а
последний первым и т.д.)
 */
public class AmazingStringTest {
    public static void main(String[] args) {
        char[] t1 = {'J', 'a', 'v', 'a', ' ', 'i', 's', ' ', 'p', 'a', 'i', 'n'};
        String t2 = "Java is pain";
        AmazingString test1 = new AmazingString(t1);
        AmazingString test2 = new AmazingString(t2);
        // проверка ссылок
        //System.out.println(test1);
        //System.out.println(test2);
        // Вернуть i-ый символ строки
        System.out.println("Вернуть i-ый символ строки");
        System.out.println(test1.printCharN(2));
        // Вернуть длину строки
        System.out.println("Вернуть длину строки");
        System.out.println(test2.stringLen());
        // Вывести строку на экран
        System.out.println("Вывести строку на экран");
        test1.printChars();
        test2.printChars();
        // Проверить, есть ли переданная подстрока в AmazingString (на вход подается массив char).
        System.out.println("Проверить, есть ли переданная подстрока в AmazingString (на вход подается массив char)");
        char[] t3 = {'i', 's'};
        System.out.println(test1.searchSubString(t3));
        // Проверить, есть ли переданная подстрока в AmazingString (на вход подается String).
        System.out.println("Проверить, есть ли переданная подстрока в AmazingString (на вход подается String)");
        String t4 = "pain";
        System.out.println(test1.searchSubString(t4));
        System.out.println("Проверить, есть ли переданная подстрока в AmazingString (её точно нет)");
        String t5 = "fun";
        System.out.println(test1.searchSubString(t5));
        // Удалить из строки AmazingString ведущие пробельные символы, если они есть
        System.out.println("Удалить из строки AmazingString ведущие пробельные символы, если они есть");
        AmazingString test3 = new AmazingString(t1);
        test3.deleteChar();
        test3.printChars();
        // Развернуть строку (первый символ должен стать последним, а последний первым и т.д.)
        System.out.println("Развернуть строку (первый символ должен стать последним, а последний первым и т.д.)");
        AmazingString test4 = new AmazingString(t2);
        test4.reversChars();
        test4.printChars();
        // доп
        System.out.println("ArrayIndexOutOfBoundsException на проверке");
        AmazingString amazingString = new AmazingString("Testest");
        amazingString.printChars();
        System.out.println(amazingString.searchSubString("sst"));
    }
}
