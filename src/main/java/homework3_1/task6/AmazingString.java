package homework3_1.task6;

public class AmazingString {
    private char[] chars;

    // Constructors
    protected AmazingString() {}

    protected AmazingString(char[] c) {
        this.chars = c;
    }

    protected AmazingString(String s) {
        this.chars = s.toCharArray();
    }

    // Getters
    protected char[] getChars() {
        return chars;
    }
    // Setters
    protected void setChars(char[] c) {
        this.chars = c;
    }

    // Methods
    /**
     * Вернуть i-ый символ строки
     */
    char printCharN(int n) {
        char out = ' ';
        if (n >= getChars().length || n < 0) {
            System.out.println("Число за пределами строки");
        } else out = getChars()[n];
        return out;
    }

    /**
     * Вернуть длину строки
     */
    int stringLen() {
        return getChars().length;
    }

    /**
     * Вывести строку на экран
     */
    void printChars() {
        for (int i = 0; i < getChars().length; i++) {
            System.out.print(getChars()[i]);
        }
        System.out.println("");
    }

    /**
     * Проверить, есть ли переданная подстрока в AmazingString
     * @param ch char[]
     * @return true / false
     */
    protected boolean searchSubString (char[] ch) {
        return checkSubString(ch);
    }

    /**
     * Проверить, есть ли переданная подстрока в AmazingString
     */
    protected boolean searchSubString(String st) {
        char[] ch = new char[st.length()];
        for (int i = 0; i < st.length(); i++) { // String in char[]
            ch[i] = st.charAt(i);
        }
        return checkSubString(ch);
    }

    /**
     * Проверить, есть ли переданная подстрока в AmazingString (на вход
     * подается массив char). Вернуть true, если найдена и false иначе
     */
    private boolean checkSubString (char[] ch) {
        boolean out = false;
        int n;
        int counter = 0;
        // найти первое совпадение
        for (int i = 0; i < getChars().length; i++) {
            if (ch[0] == getChars()[i]) {
                n = i;
                // Проверить всю подстроку
                if (n + ch.length -1 < getChars().length) {
                    for (char c : ch) {
                        if (c == getChars()[n]) {
                            counter += 1;
                        }
                        n += 1;
                    }
                    // Если вся совпадает
                    if (counter == ch.length) {
                        out = true;
                    }
                }
            }
        }
        return out;
    }

    /**
     * Удалить из строки AmazingString пробел
     */
    protected void deleteChar() {
        // Найти и посчитать символы
        int counter = 0;
        for (int i = 0; i < getChars().length; i++) {
            if (getChars()[i] == ' ') {
                counter += 1;
            }
        }
        // Собрать новый массив без пробелов
        if (counter > 0) {
            char[] tempChars = new char[getChars().length - counter];
            for (int i = 0, j = 0; i < getChars().length; i++) {
                if (getChars()[i] != ' ') {
                    tempChars[j] = getChars()[i];
                    j++;
                }
            }
            // Присвоить новый массив char
            setChars(tempChars);
        }
    }

    /**
     * Реверс строки
     */
    protected void reversChars() {
        char tmp;
        for (int i = 0, j = getChars().length -1; i < getChars().length / 2; i++, j--) {
            tmp = getChars()[i];
            getChars()[i] = getChars()[j];
            getChars()[j] = tmp;
        }
    }
}
