package homework2_1;
/*
На вход подается число N — длина массива. Затем передается массив
вещественных чисел (ai) из N элементов.
Необходимо реализовать метод, который принимает на вход полученный
массив и возвращает среднее арифметическое всех чисел массива.
Вывести среднее арифметическое на экран.
Ограничения: ● 0 < N < 100 ● 0 < ai < 1000
Пример: Входные // Выходные данные
3, 1.5 2.7 3.14 // 2.4466666666666668
2, 30.42 12 // 21.21
 */
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double[] myArray = new double[n]; // Массив длинной n
        for (int i = 0; i < myArray.length; i++) { // Заполнение массива циклом
            myArray[i] = scanner.nextDouble();
        }
        // Вывод ответа с передачей массива методу
        System.out.println(arithmeticMeanArray(myArray));
    }
    /**
     * Метод вычисляет среднеарифметическое всех чисел массива.
     * Формула: сумма всех чисел массива / на длину массива.
     */
    static double arithmeticMeanArray(double[] array) {
        double arM = 0;
        int i;
        for (i = 0; i < array.length; i++) {
            arM += array[i];
        }
        return arM / i;
    }
}
