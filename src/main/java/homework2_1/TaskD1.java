package homework2_1;
/*
Создать программу генерирующую пароль.
На вход подается число N — длина желаемого пароля. Необходимо проверить,
что N >= 8, иначе вывести на экран "Пароль с N количеством символов
небезопасен" (подставить вместо N число) и предложить пользователю еще раз
ввести число N.
Если N >= 8 то сгенерировать пароль, удовлетворяющий условиям ниже и
вывести его на экран. В пароле должны быть:
● заглавные латинские символы
● строчные латинские символы
● числа
● специальные знаки(_*-)
 */

import java.util.Scanner;

public class TaskD1 {
    public static void main(String[] args) {
        String s = passwordGenerator(); // Запуск метода генерации пароля
        System.out.println(s); // Вывод результата
    }

    /**
     * Генерирует пароль длинной 8
     *
     * @return Строку пароля
     */
    private static String passwordGenerator() {
        String outPass = "";
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите желаемую длину пароля, не менее 8.");
        int n = scanner.nextInt();
        if (n < 8) {
            System.out.println("Слишком короткий.");
            passwordGenerator();
        } else outPass = passGen(n); // Генерация пароля методом
        return outPass;
    }

    /**
     * Генерирует и проверяет пароль на сложность
     *
     * @return Строку пароля
     */
    private static String passGen(int n) {
        String outString = "";
        for (int i = 0; i < n; i++) { // Генерация и сборка пароля методом
            outString += symbolGenerator();
        }
        if (!checkPass(outString)) { // Проверка на сложность методом
            passGen(n);
        }
        return outString;
    }


    /**
     * Генерирует символ A-Z, a-z, 0-9, спец символ _*-
     *
     * @return символ A-Z, a-z, 0-9, спец символ _*-
     */
    private static char symbolGenerator() {
        final char[] SYMBOLAZUPPER = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
        final char[] SYMBOLlAZLOWER = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        final char[] SYMBOLNUM = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        final char[] SYMBOLSPEC = {'_', '*', '-'};
        char outChar = '0';
        int x = (int) (Math.random() * 4 + 1); // Выбор Массива 1-4
        if (x == 1) { // Выбор символа
            outChar = SYMBOLAZUPPER[(int) (Math.random() * 26 + 1) - 1];
        } else if (x == 2) { // Выбор символа
            outChar = SYMBOLlAZLOWER[(int) (Math.random() * 26 + 1) - 1];
        } else if (x == 3) { // Выбор символа
            outChar = SYMBOLNUM[(int) (Math.random() * 10 + 1) - 1];
        } else if (x == 4) { // Выбор символа
            outChar = SYMBOLSPEC[(int) (Math.random() * 3 + 1) - 1];
        }
        return outChar;
    }

    /**
     * Проверяет пароль на сложность
     *
     * @param password Строка пароля
     * @return true || false
     */
    private static boolean checkPass(String password) {
        int passLine = password.length(); // длинна строки
        if (passLine >= 8) { // больше 8 символов
            int lowCase, upCase, numbers, specSymbols; // переменные для условий
            lowCase = upCase = numbers = specSymbols = 0;
            char n; // переменная для символа
            for (int i = 0; i < passLine; i++) {
                n = password.charAt(i); // присвоение символа по индексу
                if (n >= 'A' && n <= 'Z') upCase = 1; // Заглавные буквы есть
                if (n >= 'a' && n <= 'z') lowCase = 1; // Строчные буквы есть
                if (n >= '0' && n <= '9') numbers = 1; // Числа есть
                if (n == '_' || n == '*' || n == '-') specSymbols = 1; // Спецзнаки есть
            }
            if (lowCase + upCase + numbers + specSymbols == 4) return true;
            else return false;
        } else return false;
    }
}
