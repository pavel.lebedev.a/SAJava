package homework2_1;
/*
 На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого передается число M.
Необходимо найти в массиве число, максимально близкое к M (т.е. такое число,
для которого |ai - M| минимальное). Если их несколько, то вывести
максимальное число.
6
-10 9 -5 -6 1 -3
-4 // -3
21
0 20
21 // 20
 */
import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] myArray = new int[n]; // Массив длинной n
        for (int i = 0; i < myArray.length; i++) { // Заполнение массива циклом
            myArray[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        sortArray(myArray);
        System.out.print(xMinArraySearch(myArray, m));
    }

    /**
     * Поиск ближайшего натурального числа к Х в однострочном массиве отсортированном по возрастанию.
     *
     * @param array однострочный массив отсортированный по возрастанию.
     * @param x     натуральное число.
     * @return Возвращает число.
     */
    static int xMinArraySearch(int[] array, int x) {
        int out = -1;
        int outIndex = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < x) {
                out = array[i];
                outIndex = i;
            }
        }
        if (outIndex + 1 < array.length ) {
            if (array[outIndex] - array[outIndex + 1] == array[outIndex + 1] - array[outIndex]) {
                System.out.print(array[outIndex + 1] + " ");
            } else if (array[outIndex] - array[outIndex + 1] < array[outIndex + 1] - array[outIndex]){
                out = array[outIndex + 1];
            }
        }
        return out;
    }

    /**
     * Сортировка массива
     *
     * @param list Массив
     */
    public static void sortArray(int[] list) {
        sortArray(list, 0, list.length - 1); // сортирует весь массив
    }

    /**
     * Сортировка массива
     *
     * @param list Массив
     * @param low
     * @param high
     */
    private static void sortArray(int[] list, int low, int high) {
        if (low < high) {
            // Найти наименьший элемент и его индекс в list(low .. high)
            int indexOfMin = low;
            int min = list[low];
            for (int i = low + 1; i <= high; i++) {
                if (list[i] < min) {
                    min = list[i];
                    indexOfMin = i;
                }
            }

            // Переставить наименьшее число в list(low .. high) и list(low)
            list[indexOfMin] = list[low];
            list[low] = min;

            // Отсортировать оставшийся list(low+1 .. high)
            sortArray(list, low + 1, high);
        }
    }
}
