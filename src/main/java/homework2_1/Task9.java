package homework2_1;
/*
На вход подается число N — длина массива. Затем передается массив
строк из N элементов (разделение через перевод строки). Каждая строка
содержит только строчные символы латинского алфавита.
Необходимо найти и вывести дубликат на экран. Гарантируется что он есть и
только один.
Ограничения:
● 0 < N < 100
● 0 < ai.length() < 1000
Пример:
Входные данные Выходные данные
4
hello
java
hi
java // java
7
today
is
the
most
most
special
day // most
 */

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[] myArray = new String[n]; // Массив длинной n
        for (int i = 0; i < myArray.length; i++) { // Заполнение массива циклом
            myArray[i] = scanner.next();
        }
        System.out.println(stringEquals(myArray));

    }

    /**
     * Сравнение строк в массиве
     * @param arr Массив
     * @return
     */
    public static String stringEquals(String[] arr) {
        String str1, str2;
        String out = "Совпадений нет";
        for (int i = 0 ; i < arr.length; i++) {
            str1 = arr[i];
            for (int j = i + 1; j < arr.length; j++) {
                str2 = arr[j];
                if (str1.compareTo(str2) == 0) {
                    out = str1;
                }
            }
        }
        return out;
    }
}
