package homework2_1;
//  На вход подается строка S, состоящая только из русских заглавных
//букв (без Ё).
//Необходимо реализовать метод, который кодирует переданную строку с
//помощью азбуки Морзе и затем вывести результат на экран. Отделять коды букв
//нужно пробелом
import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        morzeCode(input);
    }

    /**
     * Кодировка слова RU в Morze.
     *
     * @param s Слово.
     */
    static void morzeCode(String s) {
        char[] arrRU = {'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я'};
        String[] arrMorze = {".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};

        char[] arrS = s.toCharArray();
        String[] arrOut = new String[arrS.length];

        for (int i = 0; i < arrS.length; i++) {
            for (int j = 0; j < arrRU.length; j++) {
                if (arrS[i] == arrRU[j]) {
                    arrOut[i] = arrMorze[j];
                }
            }
        }
        for (int k = 0; k < arrOut.length - 1; k++) {
            System.out.print(arrOut[k] + " ");
        }
        System.out.print(arrOut[arrOut.length - 1] + "");
    }
}