package homework2_1;
/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию. После этого
вводится число X — элемент, который нужно добавить в массив, чтобы
сортировка в массиве сохранилась.
Необходимо вывести на экран индекс элемента массива, куда нужно добавить
X. Если в массиве уже есть число равное X, то X нужно поставить после уже
существующего.
Ограничения: ● 0 < N < 100 ● -1000 < ai < 1000 ● -1000 < X < 1000
Пример: Входные // Выходные данные
6, 10 20 30 40 45 60, 12 // 1
5, -1 0 2 2 3, 2 // 4
 */

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] myArray = new int[n]; // Массив длинной n
        for (int i = 0; i < myArray.length; i++) { // Заполнение массива циклом
            myArray[i] = scanner.nextInt();
        }
        int x = scanner.nextInt(); // число Х
        // Проверка методами
        int a = xArraySearch(myArray, x);
        int b = xMinArraySearch(myArray, x);

        // Вывод ответа с передачей массива методу
        if (a < 0 && b < 0) { // Если число меньше всех элементов массива, то индекс 0
            System.out.println("0");
        } else if (a >= 0) { // Если вернули индекс, выводим индекс равного +1
            System.out.println(a + 1);
        } else if (b >= 0 && b < myArray.length) { // Иначе, если вернули индекс и он в пределах массива, выводим индекс ближайшего меньшего +1
            System.out.println(b + 1);
        } else { // Иначе, последний индекс массива
            System.out.println(myArray.length - 1);
        }
    }

    /**
     * Поиск натурального числа равного Х в однострочном массиве отсортированном по возрастанию.
     *
     * @param array однострочный массив отсортированный по возрастанию.
     * @param x     натуральное число.
     * @return Возвращает индекс числа или -1.
     */
    static int xArraySearch(int[] array, int x) {
        int out = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == x) {
                out = i;
            }
        }
        return out;
    }

    /**
     * Поиск ближайшего натурального числа меньше Х в однострочном массиве отсортированном по возрастанию.
     *
     * @param array однострочный массив отсортированный по возрастанию.
     * @param x     натуральное число.
     * @return Возвращает индекс числа или -1.
     */
    static int xMinArraySearch(int[] array, int x) {
        int out = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < x) {
                out = i;
            }
        }
        return out;
    }
}
