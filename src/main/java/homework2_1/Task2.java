package homework2_1;
/*
На вход подается число N — длина массива. Затем передается массив целых чисел (ai) из N элементов.
После этого аналогично передается второй массив (aj) длины M.
Необходимо вывести на экран true, если два массива одинаковы (то есть содержат одинаковое количество
элементов и для каждого i == j элемент ai == aj). Иначе вывести false.
Ограничения: ● 0 < N < 100 ● 0 < ai < 1000 ● 0 < M < 100 ● 0 < aj < 1000
Пример: Входные // Выходные данные
7, 1 2 3 4 5 6 7, 7, 1 2 3 4 5 6 7 // true
3, 8 9 12 46, 3, 1 2 89 46 // false
1, 15, 4, 2 4 6 8 // false
 */

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] myArrayN = new int[n]; // Массив длинной N
        for (int i = 0; i < myArrayN.length; i++) { // Заполнение массива циклом
            myArrayN[i] = scanner.nextInt();
        }
        int m = scanner.nextInt();
        int[] myArrayM = new int[m]; // Массив длинной M
        for (int i = 0; i < myArrayM.length; i++) { // Заполнение массива циклом
            myArrayM[i] = scanner.nextInt();
        }
        System.out.println(compareArray(myArrayN, myArrayM)); // Вывод результата метода
    }

    /**
     * Метод проверяет равенство 1 строчных массивов, целых чисел.
     * @param array1 Первый однострочный массив целых чисел
     * @param array2 Второй однострочный массив целых чисел
     * @return Возвращает true если массивы равны, false если нет
     */
    static Object compareArray(int[] array1, int[] array2) {
        String a = "true";
        String b = "false";
        int control = 1;
        if (array1.length != array2.length) { // проверка длинны массивов, если не равно
            control = 0; // контроль 0
        } else { // если равно
            for (int i = 0; i < array1.length; i++) { // проверка всех элементов массивов
                if (array1[i] == array2[i]) { // на равенство
                    control = 1; // если равно, контроль 1
                } else { // если не равно
                    control = 0; // контроль 0, прервать программу
                    break;
                }
            }
        }
        if (control == 1) { // если контроль 1 вернуть true
            return a;
        }
        else { // иначе вернуть false
            return b;
        }
    }
}
