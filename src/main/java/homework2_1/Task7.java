package homework2_1;
/*
 На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо создать массив, полученный из исходного возведением в квадрат
каждого элемента, упорядочить элементы по возрастанию и вывести их на
экран.
6, -10 -5 1 3 3 8 // 1 9 9 25 64 100
2, -7 7 // 49 49
 */

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] myArray = new int[n]; // Массив длинной n
        for (int i = 0; i < myArray.length; i++) { // Заполнение массива циклом
            myArray[i] = scanner.nextInt();
        }
        arrMathPow(myArray, 2);
        sortArray(myArray);
        printArray(myArray);
    }

    /**
     * Возводит в степень N все числа массива
     *
     * @param arr массив
     * @param n   степень
     */
    public static void arrMathPow(int[] arr, int n) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) Math.pow(arr[i], n);
        }
    }

    /**
     * Сортировка массива
     *
     * @param list Массив
     */
    public static void sortArray(int[] list) {
        sortArray(list, 0, list.length - 1); // сортирует весь массив
    }

    /**
     * Сортировка массива
     *
     * @param list Массив
     * @param low
     * @param high
     */
    private static void sortArray(int[] list, int low, int high) {
        if (low < high) {
            // Найти наименьший элемент и его индекс в list(low .. high)
            int indexOfMin = low;
            int min = list[low];
            for (int i = low + 1; i <= high; i++) {
                if (list[i] < min) {
                    min = list[i];
                    indexOfMin = i;
                }
            }

            // Переставить наименьшее число в list(low .. high) и list(low)
            list[indexOfMin] = list[low];
            list[low] = min;

            // Отсортировать оставшийся list(low+1 .. high)
            sortArray(list, low + 1, high);
        }
    }

    /**
     * Вывод массива
     *
     * @param arr Массив
     */
    static void printArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
