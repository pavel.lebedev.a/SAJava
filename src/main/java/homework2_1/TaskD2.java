package homework2_1;
/*
Решить задачу 7 основного дз за линейное время.
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо создать массив, полученный из исходного, возведением в квадрат
каждого элемента, упорядочить элементы по возрастанию и вывести их на экран.
Ограничения: ● 0 < N < 100 ● -1000 < ai < 1000
6, -10 -5 1 3 3 8 // 1 9 9 25 64 100
2, -7 7 // 49 49

 */

import java.util.Scanner;

public class TaskD2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] myArray = new int[n]; // Массив длинной n
        for (int i = 0; i < myArray.length; i++) { // Заполнение массива циклом
            myArray[i] = scanner.nextInt();
        }
        // Возведение в степень
        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = (int) Math.pow(myArray[i], 2);
        }
        // Поиск наибольшего числа в массиве
        int max = -1;
        for (int value : myArray) {
            if (value > max) {
                max = value;
            }
        }
        // Сортировка
        // Массив размером от 0 до максимума
        int[] numCounts = new int[max + 1];
        // Заполнение массива
        for (int num : myArray) {
            numCounts[num]++;
        }
        // Массив с отсортированным
        int[] sortArray = new int[myArray.length];
        int currentSortIndex = 0;
        // Заполнение
        for (int j = 0; j < numCounts.length; j++) {
            int count = numCounts[j];
            for (int k = 0; k < count; k++) {
                sortArray[currentSortIndex] = j;
                currentSortIndex++;
            }
        }
        myArray = sortArray;
        // Печать результата
        for (int j : myArray) {
            System.out.print(j + " ");
        }
    }
}