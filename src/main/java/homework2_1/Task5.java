package homework2_1;
/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого передается число M — величина
сдвига. Необходимо циклически сдвинуть элементы массива на M элементов вправо.
Ограничения: ● 0 < N < 100 ● -1000 < ai < 1000 ● 0 <= M < 100
Пример: Входные // Выходные данные
5, 38 44 0 -11 2, 2 // -11 2 38 44 0
2, 12 15, 0 // 12 15
 */

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] myArray = new int[n]; // Массив длинной n
        for (int i = 0; i < myArray.length; i++) { // Заполнение массива циклом
            myArray[i] = scanner.nextInt();
        }
        int m = scanner.nextInt(); // Шаг смещения

        shiftNumbersBuNStepInArray(myArray, m);

        // Отображение массива
        for (int column = 0; column < myArray.length; column++) {
            System.out.print(myArray[column] + " ");
        }
    }

    /**
     * Циклически сдвигает Числа в массиве на n шагов в право
     * @param array Массив
     * @param n Шаг сдвига
     */
    static void shiftNumbersBuNStepInArray(int[] array, int n) {
        for (int i = 0; i < n ; i++) { // Применяет сдвиг на 1 шаг n раз
            shiftNumbersBuOneStepInArray(array);
        }
    }
    /**
     * Циклически сдвигает Числа в массиве на 1 шаг в право
     * @param array Массив
     */
    static void shiftNumbersBuOneStepInArray(int[] array) {
        int n = array[array.length - 1];
        for (int i = array.length - 1; i > 0; i--) {
            array[i] = array[i - 1]; // Перестановка
        }
        array[0] = n;
    }
}
