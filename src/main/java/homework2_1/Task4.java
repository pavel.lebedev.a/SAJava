package homework2_1;
/*
 На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо вывести на экран построчно сколько встретиться различных
элементов. Каждая строка должна содержать количество элементов и сам
элемент через пробел.
Ограничения: ● 0 < N < 100 ● -1000 < ai < 1000
Пример: Входные // Выходные данные
6, 7 7 7 10 26 26 // 3 7, 1 10, 2 26
2, -5 7 // 1 -5, 1 7
 */

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] myArray = new int[n]; // Массив длинной n
        for (int i = 0; i < myArray.length; i++) { // Заполнение массива циклом
            myArray[i] = scanner.nextInt();
        }

        for (int i = 0, j = 1; i < myArray.length -1; i++, j++) {
            if (myArray[i] < myArray[j]) {
                System.out.println(xArraySearchCounter(myArray, myArray[i]) + " " + myArray[i]);
            }
        }
        System.out.println(xArraySearchCounter(myArray, myArray[myArray.length - 1]) + " " + myArray[myArray.length -1]);
    }

    /**
     * Поиск счёт чисел = Х, в однострочном массиве.
     *
     * @param array однострочный массив.
     * @param x     натуральное число.
     * @return Возвращает кол-во совпадений.
     */
    static int xArraySearchCounter(int[] array, int x) {
        int out = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == x) {
                out++;
            }
        }
        return out;
    }
}

