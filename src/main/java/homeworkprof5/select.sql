--1. По идентификатору заказа получить данные заказа и данные клиента, создавшего этот заказ

/** Вложенные запросы
select
    *,
    (SELECT name FROM customers
     WHERE id = orders.customer_id) as customers,
    (SELECT telephone FROM customers
     WHERE id = orders.customer_id) as customers
from orders
where id = 2;
 */

 --v1
select *
from orders
    join customers on customer_id = customers.id
where orders.id = 2;

--v2
select date_added, flower_num, title, price, name, telephone
from orders
    join flowers on flower_id = flowers.id
    join customers on customer_id = customers.id
where orders.id = 2;

--2. Получить данные всех заказов одного клиента по идентификатору клиента за последний месяц

--v1
select *
from orders
where customer_id = 1 and (date_added >= now() - interval '30d' and date_added <= now());

--v2
select orders.id, date_added, flower_id, title, price, flower_num,
       price * flower_num as total_price
from orders
    join flowers on flower_id = flowers.id
where customer_id = 1 and (date_added >= now() - interval '30d' and date_added <= now());

--3. Найти заказ с максимальным количеством купленных цветов, вывести их название и количество

select orders.id, title, flower_num
from orders
    join flowers on flower_id = flowers.id
order by flower_num desc limit 1;

--4. Вывести общую выручку (сумму золотых монет по всем заказам) за все время

select sum(orders.flower_num * flowers.price) as gold
from orders
    join flowers on flower_id = flowers.id
