--Создание таблицы цветов flowers
create table if not exists flowers
(
    id      serial primary key,
    title   varchar(30) not null,
    price   int not null
);

--Наполнение таблицы цветов
insert into flowers(title, price)
values ('Роза',  100), ('Лилия',  50), ('Ромашка',  25);

--Создание таблицы клиентов customer
create table if not exists customers
(
    id          serial primary key,
    name        varchar(50) not null,
    telephone   varchar(12) not null
);

--Наполнение таблицы клиентов
insert into customers(name, telephone)
values ('Иван Иванов', '+79261231212'), ('Пётр Петренко', '+79104563434'), ('Хасан Абдурахматович', '+79037895656');

--Таблица заказов orders
create table if not exists orders
(
    id              serial primary key,
    customer_id     int REFERENCES customers (id) not null,
    flower_id       int REFERENCES flowers (id) not null,
    flower_num      int check (flower_num > 0 AND flower_num < 1001) not null,
    date_added      timestamp   not null
);

    --Вычесляемое поле
    --order_coast     bigint GENERATED ALWAYS AS (flower_price * flower_num) STORED

--Наполнение таблицы заказов
insert into orders(customer_id, flower_id, flower_num, date_added)
values ((select id from customers where name = 'Иван Иванов'), --customer_id
        (select id from flowers where title = 'Лилия'), --flower_id
        15, --flower_num
        now() - interval '60d'), --date_added
        ((select id from customers where name = 'Иван Иванов'), --customer_id
        (select id from flowers where title = 'Роза'), --flower_id
        7, --flower_num
        now()), --date_added
        ((select id from customers where telephone = '+79104563434'), --customer_id
        (select id from flowers where title = 'Роза'), --flower_id
        9, --flower_num
        now()), --date_added
        ((select id from customers where telephone = '+79037895656'), --customer_id
        (select id from flowers where title = 'Ромашка'), --flower_id
        33, --flower_num
        now()); --date_added

--Тест ограничения количесва
insert into orders(customer_id, flower_id, flower_num, date_added)
values (1, 1, 2000, now());

--завершения всех изменений транзакции
commit;

--отменить все изменения, внесённые начиная с момента начала транзакции или с какой-то точки сохранения (SAVEPOINT);
--очистить все точки сохранения данной транзакции;
--завершить транзакцию;
--освободить все блокировки данной транзакции.
rollback;

--проверка таблиц
select *
from flowers;

select *
from customers;

select *
from orders;



