package homework1_1;

import java.util.Scanner;

public class Tasks {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

           /*
        1. Вычислите и выведите на экран объем шара, получив его радиус r с консоли.
        Подсказка: считать по формуле V = 4/3 * pi * r^3. Значение числа pi взять из Math.
        Ограничения: 0 < r < 100
        Пример: Входные данные Выходные данные 9 = 3053.6280592892786, 25 = 65449.84694978735
         */
        System.out.print("1: ");
        int r1 = input.nextInt();
        double v1 = (4 * Math.PI * Math.pow(r1, 3)) / 3;
        System.out.println("1: " + v1);

        /*
        2. На вход подается два целых числа a и b. Вычислите и выведите среднее квадратическое a и b.
        Подсказка: Среднее квадратическое: https://en.wikipedia.org/wiki/Root_mean_square s = √[(a^2 + b^2)/2]
        Для вычисления квадратного корня воспользуйтесь функцией Math.sqrt(x)
        Ограничения: 0 < a, b < 100
        Пример: Входные данные Выходные данные 35 5 = 25.0, 23 70 = 52.100863716449076
         */
        System.out.print("2: ");
        int a2 = input.nextInt();
        int b2 = input.nextInt();
        double s2 = (Math.pow(a2, 2) + Math.pow(b2, 2)) / 2;
        s2 = Math.sqrt(s2);
        System.out.println("2: " + s2);

        /*
        3. Прочитайте из консоли имя пользователя и выведите в консоль строку: Привет, <имя пользователя>!
        Подсказка: Получите данные из консоли c помощью объекта Scanner, сохраните в переменную userName и выведите в консоль с помощью System.out.println()
        Ограничения: 0 < длина имени пользователя < 100
        Пример: Входные данные Иван Выходные данные Привет, Иван
         */
        System.out.print("3: ");
        String st = input.next();
        System.out.println("3: Привет, " + st);

        /*
        4. На вход подается количество секунд, прошедших с начала текущего дня – count. Выведите в консоль текущее время в формате: часы и минуты.
        Ограничения: 0 < count < 86400
        Пример: Входные Выходные данные 32433 = 9 0, 41812 = 11 36
         */
        System.out.print("4: ");
        int second = input.nextInt();
        int totalMinutes = second / 60;
        int totalHours = totalMinutes / 60;
        int curentMinutes = totalMinutes % 60;
        System.out.println("4: " + totalHours + " " + curentMinutes);

        /*
        5. Переведите дюймы в сантиметры (1 дюйм = 2,54 сантиметров). На вход подается количество дюймов, выведите количество сантиметров.
        Ограничения: 0 < count < 1000
        Пример: Входные Выходные данные 12 = 30.48, 99 = 251.46
         */
        System.out.print("5: ");
        int centimeters = input.nextInt();
        final double INCHTOCENTIMETERS = 2.54;
        double inch = centimeters * INCHTOCENTIMETERS;
        System.out.println("5: " + inch);

        /*
        6. На вход подается количество километров count. Переведите километры в мили (1 миля = 1,60934 км) и выведите количество миль.
        Ограничения: 0 < count < 1000
        Пример: Входные Выходные данные 7 = 4.349609156548647, 143 =88.85630134092237
         */
        System.out.print("6: ");
        int kilometers = input.nextInt();
        final double KILOMETERSTOMILES = 1.60934;
        double miles = kilometers / KILOMETERSTOMILES;
        System.out.println("6: " + miles);

        /*
        7. На вход подается двузначное число n. Выведите число, полученное перестановкой цифр в исходном числе n. Если после перестановки получается ведущий 0, его также надо вывести.
        Ограничения: 9 < count < 100
        Пример: Входные Выходные данные 45 = 54, 10 = 01
         */
        System.out.print("7: ");
        int n = input.nextInt();
        int a7 = n / 10;
        int b7 = n % 10;
        System.out.println("" + b7 + a7);

        /*
        8. На вход подается баланс счета в банке – n. Рассчитайте дневной бюджет на 30 дней.
        Ограничения: 0 < count < 100000
        Пример: Входные Выходные данные 13509 = 450.3, 81529 = 2717.633333333333
         */
        System.out.print("8: ");
        int balance = input.nextInt();
        double cash = balance / 30.0;
        System.out.println("8: " + cash);

        /*
        9. На вход подается бюджет мероприятия – n тугриков. Бюджет на одного гостя – k тугриков. Вычислите и выведите, сколько гостей можно пригласить на мероприятие.
        Ограничения: 0 < n < 100000, 0 < k < 1000, k < n
        Пример: Входные Выходные данные 14185 72 = 197, 85177 89 = 957
         */
        System.out.print("9: ");
        int n9 = input.nextInt();
        int k9 = input.nextInt();
        System.out.println("9: " + (n9 / k9));

    }
}
