package homeworkprof4;

/*
Все задачи этого блока необходимо решить, используя стримы:
На вход подается две строки.
Необходимо определить, можно ли уравнять эти две строки, применив только одну из трех
возможных операций:
1. Добавить символ
2. Удалить символ
3. Заменить символ
Пример:
“cat” “cats” -> true
“cat” “cut” -> true
“cat” “nut” -> false
 */

import java.util.Scanner;
import java.util.List;
import java.util.stream.Collectors;

public class Dop {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            String str1 = scanner.next();
            String str2 = scanner.next();

            System.out.println(check(str1, str2));
        }
    }

    private static boolean check(String first, String second) {
        int diffs = 0;
        String base;
        String comparing;

        if (first.length() >= second.length()) { // Проверка на разницу длинны строк и ориентирование элементов для последующего сравнения
            diffs = first.length() - second.length();
            base = second;
            comparing = first;
        } else {
            diffs = second.length() - first.length();
            base = first;
            comparing = second;
        }

        if (diffs > 1) return false; // Если разница больше 1 символа > false

        List<Character> diffList = base.chars().mapToObj(i -> (char)i)
                .filter(ch -> ch != comparing.charAt(base.indexOf(ch))) // Посимвольная проверка от меньшей строки
                .collect(Collectors.toList());
        diffs = diffs + diffList.size();
        if (diffs > 1) return false; // Если разница больше 1 символа > false
        return true; // Если меньше true
    }
}
