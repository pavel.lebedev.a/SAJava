package homeworkprof4;

/*
Все задачи этого блока необходимо решить, используя стримы:
3. На вход подается список строк. Необходимо вывести количество непустых строк в
списке.
Например, для List.of("abc", "", "", "def", "qqq") результат равен 3
 */

import java.util.List;

public class Task3 {
    public static void main(String[] args) {
        List <String> list = List.of("abc", "", "", "def", "qqq");
        long result = list.stream()
                .filter(s -> s.equals("")) // Фильтр по пустым строкам
                .count(); // Подсчет количества элементов
        System.out.println(result);
    }
}
