package homeworkprof4;

/*
Все задачи этого блока необходимо решить, используя стримы:
6. Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>.
 */

import java.util.Set;
import java.util.stream.Collectors;

public class Task6 {
    public static void main(String[] args) {
        Set<Set<Integer>> setSet = Set.of(Set.of(0, 1, 2), Set.of(3, 4, 5), Set.of(6, 7, 8), Set.of(9, 10, 11));
        Set<Integer> sets = setSet.stream()
                .flatMap(s -> s.stream())
                .collect(Collectors.toSet());
        sets.forEach(System.out::println);
    }
}
