package homeworkprof4;

/*
Все задачи этого блока необходимо решить, используя стримы:
5. На вход подается список непустых строк. Необходимо привести все символы строк к
верхнему регистру и вывести их, разделяя запятой.
Например, для List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ.
 */

import java.util.List;
import java.util.stream.Collectors;

public class Task5 {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "def", "qqq");
        String result = list.stream()
                .map(s -> s.toUpperCase()) // Перевод в верхний регистр
                .collect(Collectors.joining(", ")) // Больше запятых богу запятых!
                .concat("."); // И точка
        System.out.println(result);
    }
}
