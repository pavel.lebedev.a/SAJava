package homeworkprof4;

/*
Все задачи этого блока необходимо решить, используя стримы:
4. На вход подается список вещественных чисел. Необходимо отсортировать их по
убыванию.
 */

import java.util.Comparator;
import java.util.List;

public class Task4 {
    public static void main(String[] args) {
        List<Double> list = List.of(4.4, 4.3, 1.1, 2.2, 3.3);
        list.stream()
                .sorted(Comparator.reverseOrder()) // Реверсивная сортировка встроенным компаратором
                .forEach(System.out::println); // Вывод результата
    }
}
