package homeworkprof4;

/*
Все задачи этого блока необходимо решить, используя стримы:
1. Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на
экран
 */

import java.util.stream.IntStream;

public class Task1 {
    public static void main(String[] args) {
        int sum = IntStream
                .range(1, 101) // поток в диапазоне 1-100 включительно
                .filter(index -> index % 2 == 0) // фильтр по чётным
                .sum(); // сумма
        System.out.println("\n" + sum);
    }
}
