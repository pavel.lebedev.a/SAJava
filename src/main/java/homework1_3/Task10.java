package homework1_3;
/*
Вывести на экран “ёлочку” из символа звездочки (#) заданной высоты N. На N +
1 строке у “ёлочки” должен быть отображен ствол из символа |
Ограничения: 2 < n < 10
Пример: Входные Выходные данные
3
  #
 ###
#####
  |

6
     #
    ###
   #####
  #######
 #########
###########
    |
 */

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        /*
        Писал в бреду. Лишняя пустая строка в начале

        for (int i = n, j = 0; i >= 0; --i, j++) {
            for (int k = 0; k < i; k++) {
                System.out.print(' ');
            }
            for (int l = 0; l < (j * 2) - 1; l++) {
                System.out.print('#');
            }
            System.out.println();
        }
        for (int i = 0; i < n -1; i++) {
            System.out.print(' ');
        }
        System.out.print('|');
         */

        /*
        int i = 0;
        int p = n-1;
        while (i < n)
        { // Бредовы состояния опасны, написал, а что не понял, но бот съел.
            String e = new String(new char[1 + i *2]).replace("\0", "#");
            String space = new String(new char[p]).replace("\0", " ");
            System.out.printf(space + e + "\n");
            ++i;
            --p;
        }
        for (i = 0; i < n -1; i++) {
            System.out.print(' ');
        }
        System.out.print('|');
         */

        for (int i = 1; i <= n; i++) {
            for (int j = n - i; j > 0; j--) System.out.print(" ");
            for (int j = 0; ++j < i * 2;) System.out.print("#");
            System.out.println();
        }
        for (int i = 0; i < n - 1; i++) System.out.print(" ");
        System.out.print("|");
    }
}