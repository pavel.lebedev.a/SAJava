package homework1_3;
/*
Дано натуральное число n. Вывести его цифры в “столбик”.
Ограничения: 0 < n < 1000000
Пример:
Входные данные Выходные данные
74              7
                4
1630            1
                6
                3
                0
 */
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s = input.nextLine();
        //System.out.println(s.length() + " " + s);
        for (int i = 0; i < s.length(); i++) {
            System.out.println(s.charAt(i));
        }
    }
}
