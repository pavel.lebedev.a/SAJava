package homework1_3;
/*
В банкомате остались только купюры номиналом 8 4 2 1. Дано положительное
число n - количество денег для размена. Необходимо найти минимальное
количество купюр с помощью которых можно разменять это количество денег
(соблюсти порядок: первым числом вывести количество купюр номиналом 8,
вторым - 4 и т д)
Ограничения: 0 < n < 1000000
Пример: Входные - Выходные данные
51 - 6 0 1 1
10 - 1 0 1 0
60 - 7 1 0 0
 */

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int sum = input.nextInt();

        int remSum = sum;

        int bill8 = remSum / 8;
        remSum = remSum % 8;

        int bill4 = remSum / 4;
        remSum = remSum % 4;

        int bill2 = remSum / 2;
        remSum = remSum % 2;

        int bill1 = remSum;
        System.out.print(bill8 + " " + bill4 + " " + bill2 + " " + bill1);
    }
}