package homework1_3;

/*
Напечатать таблицу умножения от 1 до 9. Входных данных нет. Многоточие в
примере ниже подразумевает вывод таблицы умножения и для остальных чисел
2, 3 и т. д
 */
public class Task1 {
    public static void main(String[] args) {
        int a, b;
        for (a = 1; a < 10; a++) {
            for (b = 1; b < 10; b++) {
                System.out.println(a + " x " + b + " = " + (a * b));
            }
        }
    }
}
