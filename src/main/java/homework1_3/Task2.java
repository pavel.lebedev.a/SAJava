package homework1_3;
/*
На вход подается два положительных числа m и n. Найти сумму чисел между m и n включительно.
Ограничения: 0 < m, n < 10, m < n
Пример: 7 9 out.print 24; 1 2 out.print 3
 */

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m = input.nextInt();
        int n = input.nextInt();

        for (int i = m + 1; i < n; i++) {
            m += i;
        }
        System.out.println(m + n);
    }
}
