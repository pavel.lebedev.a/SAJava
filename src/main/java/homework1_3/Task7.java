package homework1_3;
/*
Дана строка s. Вычислить количество символов в ней, не считая пробелов
(необходимо использовать цикл).
Ограничения: 0 < s.length() < 1000
Пример: Входные - Выходные данные
Hello world - 10
Never give up - 11
 */

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s = input.nextLine();
        int n = 0;
        char m = ' ';
        for (int i = 0; i < s.length(); i++) {      // считать пока не кончится строка
            if (s.charAt(i) != m) {                 // если символ не пробел
                n++;                                // то n+1
            }
        }
        System.out.println(n);
    }
}
