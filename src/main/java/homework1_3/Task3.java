package homework1_3;
/*
На вход подается два положительных числа m и n. Необходимо вычислить
m^1 + m^2 + ... + m^n
Ограничения:    0 < m, n < 10
Пример:
Входные данные Выходные данные
1 1             1
8 5             37448
 */

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m = input.nextInt();
        int n = input.nextInt();
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            sum += (int) Math.pow(m, i);
            //System.out.println("p sum = " + sum);
            //System.out.println("p m = " + m);
            //System.out.println("p i = " + i);
        }
        //System.out.println("i SUM = " + sum);
        System.out.println(sum);
    }
}
