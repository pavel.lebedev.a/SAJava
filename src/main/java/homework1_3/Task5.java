package homework1_3;
/*
Даны положительные натуральные числа m и n. Найти остаток от деления m на
n, не выполняя операцию взятия остатка.
Ограничения: 0 < m, n < 10
Входные - Выходные данные
9 1 - 0
8 3 - 2
7 9 - 7
 */

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m = input.nextInt();
        int n = input.nextInt();
        //System.out.println(m % n);
        System.out.println(m - (m/n)*n);
    }
}
