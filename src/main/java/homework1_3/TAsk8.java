package homework1_3;
/*
На вход подается: целое число n, целое число p, целые числа a1, a2 , … an
Необходимо вычислить сумму всех чисел a1, a2, a3 … an которые строго больше p.
Ограничения: 0 < m, n, ai < 1000
Пример:
Входные - Выходные данные
2, 18, 95 31 - 126
6, 29, 40 37 97 72 80 18 - 326
1, 100, 42 - 0
 */
import java.util.Scanner;

public class TAsk8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int p = input.nextInt();
        int[] arNumA = new int[n]; // массив для a1, a2, a3 … an
        int sum = 0;
        for (int i = 0; i < arNumA.length ; i++) { // заполнение массива с консоли
            arNumA[i] = input.nextInt();
        }
        for (int i = 0; i < arNumA.length; i++) { // поиск и сумма чисел > p
            if (arNumA[i] > p) {
                sum += arNumA[i];
            }
        }
        System.out.println(sum); // Вывод
    }
}
