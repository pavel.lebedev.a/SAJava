package homeworkprof2;
/*
В некоторой организации хранятся документы (см. класс Document). Сейчас все
документы лежат в ArrayList, из-за чего поиск по id документа выполняется
неэффективно. Для оптимизации поиска по id, необходимо помочь сотрудникам
перевести хранение документов из ArrayList в HashMap.
public class Document {
    public int id;
    public String name;
    public int pageCount;
}
Реализовать метод со следующей сигнатурой:
public Map<Integer, Document> organizeDocuments(List<Document> documents)
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.IntStream;

public class Task4 {
    public static void main(String[] args) {
        List<Document> documents = new ArrayList<>();
        documents.add(new Document(0, "qwer", 5));
        documents.add(new Document(1, "asd", 6));
        documents.add(new Document(4, "zxc", 4));
        documents.add(new Document(2, "qaz", 2));
        documents.add(new Document(3, "wsx", 5));

        Map<Integer, Document> documentMap = organizeDocuments(documents);

        IntStream.range(0, documentMap.size()).mapToObj(
                i -> documentMap.get(i).getId() + " "
                + documentMap.get(i).getName() + " "
                + documentMap.get(i).getPageCount())
                .forEach(System.out::println);
    }

    /* перевести хранение документов из ArrayList в HashMap */
    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> documentMap = new TreeMap<>();
        for (Document document : documents) {
            documentMap.put(document.getId(), document);
        }
        return documentMap;
    }
}
