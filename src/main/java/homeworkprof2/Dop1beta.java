package homeworkprof2;
/*
Реализовать метод, который принимает массив words и целое положительное число k.
Необходимо вернуть k наиболее часто встречающихся слов.
Результирующий массив должен быть отсортирован по убыванию частоты
встречаемого слова. В случае одинакового количества частоты для слов, то
отсортировать и выводить их по убыванию в лексикографическом порядке.
Пример:Входные данные
words = ["the","day","is","sunny","the","the","the","sunny","is","is","day"]
k = 4
Выходные данные
["the","is","sunny","day"]
 */

import java.util.*;

public class Dop1beta {
    public static void main(String[] args) {
        String[] words = {"the","day","is","sunny","the","the","the","sunny","is","is","day"};
        int k = 3;
        sort(words, k);
    }

    public static void sort (String[] words, int k) {
        //Arrays.sort(words); // Сортировка массива строк

        // ПРОВЕРКА МАССИВА !!!!!!!!!!
        System.out.println("ПРОВЕРКА МАССИВА");
        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i].toString());
        }

        // Добавить слова в map как ключ
        Map<String, Integer> wordCountMap = new HashMap<>();
        for (int i = 0; i < words.length; i++) {
            wordCountMap.put(words[i], 0);
        }

        // ПРОАВЕРКА МАПЫ !!!!!!!!!!!!
        System.out.println("ПРОВЕРКА МАПЫ");
        for(Map.Entry<String, Integer> entry: wordCountMap.entrySet()) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }

        // Пройтись, по массиву и посчитать количество совпадений, записать как значение в map
        for (int i = 0; i < words.length; i++) {
            wordCountMap.put(words[i], wordCountMap.get(words[i]) + 1);
        }

        // ПРОАВЕРКА МАПЫ 2 !!!!!!!!!!!!
        System.out.println("ПРОВЕРКА МАПЫ");
        for(Map.Entry<String, Integer> entry: wordCountMap.entrySet()) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }

        // Сортировка по значению
        List <Map.Entry<String, Integer>> valuesList = new ArrayList(wordCountMap.entrySet());
        Collections.sort(valuesList, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o1.getValue().compareTo(o2.getValue());
            }
        });
        Collections.sort(valuesList, Comparator.comparing(Map.Entry::getValue));

        // ПРОВЕРКА СПИСКА !!!!!!!!!!
        System.out.println("ПРОВЕРКА СПИСКА");
        valuesList.forEach(System.out::println);

        // list to map
        Map<String, Integer> numCountMap = new HashMap<>();
        for (int i = 0; i < valuesList.size(); i++) {
            numCountMap.put(valuesList.get(i).getKey(), valuesList.get(i).getValue());
        }

        // ПРОАВЕРКА МАПЫ 3 !!!!!!!!!!!!
        System.out.println("ПРОВЕРКА МАПЫ");
        for(Map.Entry<String, Integer> entry: numCountMap.entrySet()) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
        }

        // Вывести результат по задаче
        for(Map.Entry<String, Integer> entry: numCountMap.entrySet()) {
            if (k >= entry.getValue())
            System.out.println(entry.getKey());
        }

    }
}
