package homeworkprof2;
/*
С консоли на вход подается две строки s и t. Необходимо вывести true, если
одна строка является валидной анаграммой другой строки и false иначе.
Анаграмма — это слово или фраза, образованная путем перестановки букв
другого слова или фразы, обычно с использованием всех исходных букв ровно
один раз.
 */

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        String t = scanner.nextLine();
        System.out.println(isAnagram(s, t));
    }

    /** Проверка строк на Анаграмму, можно и коллекциями, но так мне кажется проще, надёжнее и быстрее */
    public static boolean isAnagram (String s, String t) {
        s = s.toLowerCase();
        t = t.toLowerCase();
        int sumS = 0, sumT = 0;
        if (s.length() != t.length()) {
            return false;
        } else {
            for (int i = 0; i < s.length(); i++) {
                sumS += s.charAt(i);
                sumT += t.charAt(i);
            }
            return sumS == sumT;
        }
    }
}
