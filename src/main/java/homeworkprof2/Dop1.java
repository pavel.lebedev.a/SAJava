package homeworkprof2;
/*
Реализовать метод, который принимает массив words и целое положительное число k.
Необходимо вернуть k наиболее часто встречающихся слов.
Результирующий массив должен быть отсортирован по убыванию частоты
встречаемого слова. В случае одинакового количества частоты для слов, то
отсортировать и выводить их по убыванию в лексикографическом порядке.
Пример:Входные данные
words = ["the","day","is","sunny","the","the","the","sunny","is","is","day"]
k = 4
Выходные данные
["the","is","sunny","day"]
 */

import java.util.*;
import java.util.stream.Collectors;

public class Dop1 {
    public static void main(String[] args) {
        String[] words = {"the","day","is","sunny","the","the","the","sunny","is","is","day"};
        int k = 3;
        sort(words, 4);
    }

    public static void sort (String[] words, int k) {

        // Добавить слова в map как ключ
        Map<String, Integer> wordCountMap = new HashMap<>();
        for (int i = 0; i < words.length; i++) {
            wordCountMap.put(words[i], 0);
        }

        // Пройтись, по массиву и посчитать количество совпадений, записать как значение в map
        for (int i = 0; i < words.length; i++) {
            wordCountMap.put(words[i], wordCountMap.get(words[i]) + 1);
        }

        // Вывести результат по задаче
        for(Map.Entry<String, Integer> entry: wordCountMap.entrySet()) {
            if (k >= entry.getValue())
            System.out.println(entry.getKey());
        }
    }
}
