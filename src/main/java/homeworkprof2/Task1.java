package homeworkprof2;
/*
Реализовать метод, который на вход принимает ArrayList<T>, а возвращает
набор уникальных элементов этого массива. Решить используя коллекции
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Task1 {
    public static void main(String[] args) {
        ArrayList<String> arrayList1 = new ArrayList<>();
        arrayList1.add("QweR");
        arrayList1.add("QweR");
        arrayList1.add("QweR");
        arrayList1.add("qaz");
        arrayList1.add("wsx");
        System.out.println("Before: " + arrayList1.size());
        ArrayList<String> arrayList2 = returnUnique(arrayList1);
        System.out.println("After: " + arrayList2.size());

        ArrayList<Integer> arrayList3 = new ArrayList<>();
        arrayList3.add(13);
        arrayList3.add(13);
        arrayList3.add(13);
        arrayList3.add(18);
        arrayList3.add(18);
        System.out.println("Before: " + arrayList3.size());
        ArrayList<Integer> arrayList4 = returnUnique(arrayList3);
        System.out.println("After: " + arrayList4.size());
    }

    public static <T> ArrayList<T> returnUnique(ArrayList<T> arrayList) {
        Set<T> unique = new HashSet<T>(arrayList);
        return new ArrayList<>(unique);
    }
}
