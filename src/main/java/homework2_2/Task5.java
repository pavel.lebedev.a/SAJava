package homework2_2;
/*
На вход подается число N — количество строк и столбцов матрицы.
Затем передается сама матрица, состоящая из натуральных чисел.
Необходимо вывести true, если она является симметричной относительно
побочной диагонали, false иначе.
Побочной диагональю называется диагональ, проходящая из верхнего правого
угла в левый нижний.
Ограничения: ● 0 < N < 100 ● 0 < ai < 1000
Пример: Входные данные // Выходные данные
3
1 2 3
4 5 6
7 8 9
// false
5
57 190 160 71 42
141 79 187 19 71
141 16 7 187 160
100 42 16 79 190
15 100 141 141 57
// true
 */

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] myMatrix = new int[n][n]; // Матрица длинной n * n
        for (int i = 0; i < myMatrix.length; i++) { // Заполнение матрицы с консоли
            for (int j = 0; j < myMatrix[i].length; j++) {
                myMatrix[i][j] = scanner.nextInt();
            }
        }
        System.out.println(sideDiagonalSymmetry(myMatrix));
    }

    /**
     * ПРАВИЛЬНАЯ Проверка на симметрию: Побочной диагональю квадратной матрицы
     * Сравнение от правого верхнего угла, правой грани с верхней.
     *
     * @param matrix матрица
     * @return true / false
     */
    private static boolean sideDiagonalSymmetry(int[][] matrix) {
        boolean out = true;
        for (int i = 0, j = matrix.length - 1; i < matrix.length; i++, j--) {
            if (matrix[i][matrix.length - 1] != matrix[0][j]) {
                out = false;
            }
        }
        return out;
    }

//    // Или бот не прав или ТЗ написано неверно.
//    /**
//     * Проверка на симметрию: от 0*0 к n*n
//     * @param matrix матрица
//     * @return true / false
//     */
//    private static boolean sideDiagonalSymmetry(int[][] matrix) {
//        boolean out = true;
//        for (int i = 0, j = matrix.length - 1; i < matrix.length / 2 && j >= matrix.length / 2; i++, j--) {
//            if (matrix[i][i] != matrix[j][j]) {
//                out = false;
//            }
//        }
//        return out;
//    }
//    // Побочной диагонали квадратной матрицы называется диагональ, идущая из правого верхнего угла в левый нижний угол.
//    /**
//     * Проверка на Симметрию: Побочной диагонали квадратной матрицы
//     * @param matrix матрица
//     * @return true / false
//     */
//    private static boolean sideDiagonalSymmetry(int[][] matrix) {
//        boolean out = true;
//        for (int i = 0, j = matrix.length - 1; i < matrix.length / 2 && j >= matrix.length / 2; i++, j--) {
//            if (matrix[i][j] != matrix[j][i]) {
//                out = false;
//            }
//        }
//        return out;
//    }
}
