package homework2_2;
/*
Раз в год Петя проводит конкурс красоты для собак. К сожалению, система
хранения участников и оценок неудобная, а победителя определить надо. В
первой таблице в системе хранятся имена хозяев, во второй - клички животных,
в третьей — оценки трех судей за выступление каждой собаки. Таблицы
связаны между собой только по индексу. То есть хозяин i-ой собаки указан в i-ой
строке первой таблицы, а ее оценки — в i-ой строке третьей таблицы. Нужно
помочь Пете определить топ 3 победителей конкурса.
На вход подается число N — количество участников конкурса. Затем в N
строках переданы имена хозяев. После этого в N строках переданы клички
собак. Затем передается матрица с N строк, 3 вещественных числа в каждой —
оценки судей. Победителями являются три участника, набравшие
максимальное среднее арифметическое по оценкам 3 судей. Необходимо
вывести трех победителей в формате “Имя хозяина: кличка, средняя оценка”.
Гарантируется, что среднее арифметическое для всех участников будет
различным.
Ограничения: 0 < N < 100
Пример: Входные данные // Выходные данные
4
Иван
Николай
Анна
Дарья
Жучка
Кнопка
Цезарь
Добряш
7 6 7
8 8 7
4 5 6
9 9 9
//
Дарья: Добряш, 9.0
Николай: Кнопка, 7.6
Иван: Жучка, 6.6
 */

import java.util.Scanner;
// кажется это должно было быть как-то проще
public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String[] arrayName = new String[n]; // Массив имён длинной n
        for (int i = 0; i < arrayName.length; i++) { // Заполнение массива
            arrayName[i] = scanner.next();
        }
        String[] arrayDogName = new String[n]; // Массив имён собак длинной n
        for (int i = 0; i < arrayDogName.length; i++) { // Заполнение массива
            arrayDogName[i] = scanner.next();
        }
        int[][] grades = new int[n][3]; // Матрица оценок n * 3
        for (int i = 0; i < grades.length; i++) { // Заполнение матрицы с консоли
            for (int j = 0; j < grades[i].length; j++) {
                grades[i][j] = scanner.nextInt();
            }
        }
        double[] finalGrades = new double[n]; // Массив средней оценки
        for (int i = 0; i < grades.length; i++) {
            double sum = 0.0;
            for (int j = 0; j < grades[i].length; j++) { // подсчёт суммы оценок
                sum += grades[i][j];
            }
            finalGrades[i] = (double) ((int) ((sum / 3) * 10)) / 10; // Запись средней оценки
        }
        double[] sortFinalGrades = new double[finalGrades.length]; // Массив для отсортированных оценок
        for (int i = 0; i < sortFinalGrades.length; i++) { // Заполнение массива
            sortFinalGrades[i] = finalGrades[i];
        }
        sortArray(sortFinalGrades); // сортировка

        for (int i = sortFinalGrades.length - 1; i > sortFinalGrades.length - 4; i--) { // 3 раза, от большего к меньшему
            int m = xArraySearch(finalGrades, sortFinalGrades[i]); //Поиск совпадений  в отсортированном и финальном массиве
            System.out.println(arrayName[m] + ": " + arrayDogName[m] + ", " + finalGrades[m]); // Вывод победителей
        }
    }

    /**
     * Сортировка массива
     *
     * @param list Массив
     */
    public static void sortArray(double[] list) {
        sortArray(list, 0, list.length - 1); // сортирует весь массив
    }

    /**
     * Сортировка массива
     *
     * @param list Массив
     * @param low
     * @param high
     */
    private static void sortArray(double[] list, int low, int high) {
        if (low < high) {
            // Найти наименьший элемент и его индекс в list(low .. high)
            int indexOfMin = low;
            double min = list[low];
            for (int i = low + 1; i <= high; i++) {
                if (list[i] < min) {
                    min = list[i];
                    indexOfMin = i;
                }
            }

            // Переставить наименьшее число в list(low .. high) и list(low)
            list[indexOfMin] = list[low];
            list[low] = min;

            // Отсортировать оставшийся list(low+1 .. high)
            sortArray(list, low + 1, high);
        }
    }

    /**
     * Поиск числа равного Х в однострочном массиве отсортированном по возрастанию.
     *
     * @param array однострочный массив отсортированный по возрастанию.
     * @param x     натуральное число.
     * @return Возвращает индекс числа или -1.
     */
    static int xArraySearch(double[] array, double x) {
        int out = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == x) {
                out = i;
            }
        }
        return out;
    }
}
