package homework2_2;
/*
На вход подается число N — количество строк и столбцов матрицы. Затем в
последующих двух строках подаются координаты X (номер столбца) и Y (номер
строки) точек, которые задают прямоугольник.
Необходимо отобразить прямоугольник с помощью символа 1 в матрице,
заполненной нулями (см. пример) и вывести всю матрицу на экран.
Ограничения:
● 0 < N < 100 ● 0 <= X1, Y1, X2, Y2 < N ● X1 < X2 ● Y1 < Y2
Пример: Входные данные Выходные данные
7
1 2
3 4
0 0 0 0 0 0 0
0 0 0 0 0 0 0
0 1 1 1 0 0 0
0 1 0 1 0 0 0
0 1 1 1 0 0 0
0 0 0 0 0 0 0
0 0 0 0 0 0 0
5
1 0
4 1
0 1 1 1 1
0 1 1 1 1
0 0 0 0 0
0 0 0 0 0
0 0 0 0 0
 */

import java.util.Arrays;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] myMatrix = new int[n][n]; // Матрица длинной n * n
        for (int[] matrix : myMatrix) { // Заполнение матрицы циклом
            Arrays.fill(matrix, 0);
        }
        int x1, y1, x2, y2; // X (номер столбца) и Y (номер строки), myMatrix[y][x]
        x1 = scanner.nextInt();
        y1 = scanner.nextInt();
        x2 = scanner.nextInt();
        y2 = scanner.nextInt();
        for (int i = y1; i <= y2; i++) {
            myMatrix[i][x1] = 1;
            myMatrix[i][x2] = 1;
        }
        for (int i = x1 + 1; i < x2; i++) {
            myMatrix[y1][i] = 1;
            myMatrix[y2][i] = 1;
        }
        printMatrix(myMatrix); // Check Matrix !!!
    }

    /**
     * Вывод Массива
     *
     * @param myMatrix Массив
     */
    private static void printMatrix(int[][] myMatrix) {
        for (int i = 0; i < myMatrix.length; i++) { // Check Matrix !!!
            for (int j = 0; j < myMatrix[i].length; j++) {
                System.out.print(myMatrix[i][j] + " ");
            }
            System.out.println("");
        }
    }
}
