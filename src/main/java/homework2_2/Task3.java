package homework2_2;
/*
На вход подается число N — количество строк и столбцов матрицы. Затем
передаются координаты X и Y расположения коня на шахматной доске.
Необходимо заполнить матрицу размера NxN нулями, местоположение коня
отметить символом K, а позиции, которые он может бить, символом X.
Ограничения:
● 4 < N < 100 ● 0 <= X, Y < N
Пример: Входные данные // Выходные данные
5 0 4 //
0 0 0 0 0
0 0 0 0 0
0 X 0 0 0
0 0 X 0 0
K 0 0 0 0

7 3 3 //
0 0 0 0 0 0 0
0 0 X 0 X 0 0
0 X 0 0 0 X 0
0 0 0 K 0 0 0
0 X 0 0 0 X 0
0 0 X 0 X 0 0
0 0 0 0 0 0 0
 */

import methods.MatrixMethods;

import java.util.Arrays;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        char[][] myMatrix = new char[n][n]; // Матрица длинной n * n
        for (char[] matrix : myMatrix) { // Заполнение матрицы циклом
            Arrays.fill(matrix, '0');
        }
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        knightsMove(myMatrix, x, y);
        printMatrix(myMatrix, n, n);
    }

    private static void knightsMove(char[][] myMatrix, int x, int y) {
        myMatrix[y][x] = 'K'; // Knights
        int[] xMove = {1, -1, -2, -2, 1, -1, 2, 2};
        int[] yMove = {-2, -2, 1, -1, 2, 2, 1, -1};
        for (int i = 0; i < xMove.length; i++) {
            if (y + yMove[i] > 0 && x + xMove[i] > 0 && y + yMove[i] < myMatrix.length && x + xMove[i] < myMatrix.length) {
                myMatrix[y + yMove[i]][x + xMove[i]] = 'X';
            }
        }
    }

    /**
     * Вывод Массива без лишних пробелов
     *
     * @param myMatrix Массив
     */
    private static void printMatrix(char[][] myMatrix, int n, int m) {
        int i = n - 1, j = m - 1;
        for (i = 0; i < myMatrix.length - 1; i++) {
            for (j = 0; j < myMatrix[i].length - 1; j++) {
                System.out.print(myMatrix[i][j] + " ");
            }
            System.out.print(myMatrix[i][j]);
            System.out.println("");
        }
        for (j = 0; j < myMatrix[i].length - 1; j++) {
            System.out.print(myMatrix[i][j] + " ");
        }
        System.out.print(myMatrix[i][j]);
    }
}
