package homework2_2;
/*
На вход подается число N. Необходимо вывести цифры числа справа налево.
Решить задачу нужно через рекурсию.
Ограничения: 0 < N < 1000000
Пример: Входные данные // Выходные данные
12374 // 4 7 3 2 1
201 // 1 0 2
 */

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        printsNumReverse(n);
    }

    /** Реверсивно печатает цифры целого числа
     *
     * @param n Целое число
     */
    private static void printsNumReverse(int n) {
        System.out.print(n % 10 + " ");
        if (n >= 10) {
            printsNumReverse(n / 10);
        }
    }
}
