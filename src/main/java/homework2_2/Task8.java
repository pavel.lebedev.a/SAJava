package homework2_2;
/*
На вход подается число N. Необходимо посчитать и вывести на экран сумму его
цифр. Решить задачу нужно через рекурсию.
Ограничения: 0 < N < 1000000
Пример: Входные данные // Выходные данные
12374 // 17
201 // 3
 */

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(recursiveNumberSum(n));
    }

    /**
     * Метод находит сумму чисел целого числа с Рекурсией.
     *
     * @param n Целое число
     * @return Сумму чисел целого числа
     */
    private static int recursiveNumberSum(int n) {
        if (n / 10 >= 1) {
            int temp = n % 10;
            int remainder = n / 10;
            return temp + recursiveNumberSum(remainder);
        } else return n;
    }
}
