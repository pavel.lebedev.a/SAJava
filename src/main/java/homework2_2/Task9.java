package homework2_2;
/*
На вход подается число N. Необходимо вывести цифры числа слева направо.
Решить задачу нужно через рекурсию.
Ограничения: 0 < N < 1000000
Пример: Входные данные // Выходные данные
12374 // 1 2 3 7 4
201 // 2 0 1
 */

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        printsNum(n);
    }

    /** Печатает цифры целого числа
     *
     * @param n Целое число
     */
    private static void printsNum(int n) {
        int remainder = n % 10;
        if (n == 0) {
            return;
        } else {
            printsNum(n / 10);
        }
        System.out.print(remainder + " ");
    }
}
