package homework2_2;
/*
На вход передается N — количество столбцов в двумерном массиве и M —
количество строк. Затем сам передается двумерный массив, состоящий из
натуральных чисел.
Необходимо сохранить в одномерном массиве и вывести на экран
минимальный элемент каждой строки
Ограничения: ● 0 < N < 100 ● 0 < M < 100 ● 0 < ai < 1000
Пример: Входные данные // Выходные данные
3 2
10 20 15
7 5 9
// 10 5
1 3
30
42
15
// 30 42 15
 */

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt(); // columns
        int m = scanner.nextInt(); // rows
        int[] myArray = new int[n * m];
        for (int i = 0; i < myArray.length; i++) { // Заполнение массива циклом
            myArray[i] = scanner.nextInt();
        }
        // System.out.println(Arrays.toString(myArray)); // Check Array !!!
        int[][] myMatrix = new int[m][n]; // Матрица длинной n * m
        for (int i = 0, x = 0; i < myMatrix.length && x < myArray.length; i++) { // Заполнение матрицы циклом
            for (int j = 0; j < myMatrix[i].length; j++) {
                myMatrix[i][j] = myArray[x];
                x++;
            }
        }
        // printMatrix(myMatrix); // Check Matrix !!!
        for (int i = 0; i < myMatrix.length; i++) { // Sort Matrix
            sortArray(myMatrix[i]);
        }
        // printMatrix(myMatrix); // Check Matrix !!!
        int i;
        for (i = 0; i < myMatrix.length - 1; i++) {
            System.out.print(myMatrix[i][0] + " ");
        }
        System.out.print(myMatrix[i][0]);
    }

    /**
     * Сортировка массива
     *
     * @param list Массив
     */
    public static void sortArray(int[] list) {
        sortArray(list, 0, list.length - 1); // сортирует весь массив
    }

    /**
     * Сортировка массива
     *
     * @param list Массив
     * @param low
     * @param high
     */
    private static void sortArray(int[] list, int low, int high) {
        if (low < high) {
            // Найти наименьший элемент и его индекс в list(low .. high)
            int indexOfMin = low;
            int min = list[low];
            for (int i = low + 1; i <= high; i++) {
                if (list[i] < min) {
                    min = list[i];
                    indexOfMin = i;
                }
            }

            // Переставить наименьшее число в list(low .. high) и list(low)
            list[indexOfMin] = list[low];
            list[low] = min;

            // Отсортировать оставшийся list(low+1 .. high)
            sortArray(list, low + 1, high);
        }
    }

    /**
     * Вывод Массива
     *
     * @param myMatrix Массив
     */
    private static void printMatrix(int[][] myMatrix) {
        for (int i = 0; i < myMatrix.length; i++) { // Check Matrix !!!
            for (int j = 0; j < myMatrix[i].length; j++) {
                System.out.print(myMatrix[i][j] + " ");
            }
            System.out.println("");
        }
    }
}
