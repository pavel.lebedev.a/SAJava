package homework2_2;
/*
На вход подается число N — количество строк и столбцов матрицы. Затем
передается сама матрица, состоящая из натуральных чисел. После этого
передается натуральное число P.
Необходимо найти элемент P в матрице и удалить столбец и строку его
содержащий (т.е. сохранить и вывести на экран массив меньшей размерности).
Гарантируется, что искомый элемент единственный в массиве.
Ограничения: ● 0 < N < 100 ● 0 < ai < 1000
3
1 2 3
1 7 3
1 2 3
7
//
1 3
1 3

4
1 2 3 4
1 2 3 4
1 2 3 4
1 2 3 5
5
//
1 2 3
1 2 3
1 2 3
 */

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] myMatrix = new int[n][n]; // Матрица длинной n * n
        for (int i = 0; i < myMatrix.length; i++) { // Заполнение матрицы с консоли
            for (int j = 0; j < myMatrix[i].length; j++) {
                myMatrix[i][j] = scanner.nextInt();
            }
        }
        int p = scanner.nextInt();

        int[] ijArray = xMatrixSearchCounter(myMatrix, p);

        if (ijArray[0] != -1 || ijArray[1] != -1) {
            int[][] newMyMatrix = new int[n - 1][n - 1];
            for (int i = 0, k = 0; i < myMatrix.length; i++, k++) {
                if (i != ijArray[0]) {
                    for (int j = 0, l = 0; j < myMatrix.length; j++, l++) {
                        if (j != ijArray[1]) {
                            newMyMatrix[k][l] = myMatrix[i][j];
                        } else l--;
                    }
                } else k--;
            }
            printMatrix(newMyMatrix, n - 1, n - 1); // вывод итоговой матрицы
        }
    }

    /**
     * Ищет число х в матрице, перебором.
     *
     * @param matrix Матрица.
     * @param x      Искомое число.
     * @return Массив[2] с координатами y/x числа
     */
    private static int[] xMatrixSearchCounter(int[][] matrix, int x) {
        int[] out = new int[2];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (x == matrix[i][j]) {
                    out[0] = i;
                    out[1] = j;
                }
            }
        }
        return out;
    }

    /**
     * Вывод Массива без лишних пробелов
     *
     * @param myMatrix Массив
     */
    private static void printMatrix(int[][] myMatrix, int n, int m) {
        int i = n - 1, j = m - 1;
        for (i = 0; i < myMatrix.length - 1; i++) {
            for (j = 0; j < myMatrix[i].length - 1; j++) {
                System.out.print(myMatrix[i][j] + " ");
            }
            System.out.print(myMatrix[i][j]);
            System.out.println("");
        }
        for (j = 0; j < myMatrix[i].length - 1; j++) {
            System.out.print(myMatrix[i][j] + " ");
        }
        System.out.print(myMatrix[i][j]);
    }
}
