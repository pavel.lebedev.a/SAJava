package homework3_2;

class Book {
    private String bookAuthor;
    private String bookName;
    private boolean status; // Статус, свободна true / одолжена false
    private double rate; // Средняя оценка
    private int rateNum; // Количество оценок

    // Setters and Getters
    protected String getBookAuthor() {
        return bookAuthor;
    }

    protected void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    protected String getBookName() {
        return bookName;
    }

    protected void setBookName(String bookName) {
        this.bookName = bookName;
    }

    protected boolean getStatus() {
        return status;
    }

    protected void setStatus(boolean status) {
        this.status = status;
    }

    protected double getRate() {
        return rate;
    }

    protected void setRate(double rate) {
        this.rate = rate;
    }

    protected int getRateNum() {
        return rateNum;
    }

    protected void setRateNum(int rateNum) {
        this.rateNum = rateNum;
    }

    // Constructors
    protected Book () {}

    /**
     * @param bookAuthor Автор
     * @param bookName Название
     * @param status свободна true / одолжена false
     * @param rate Средняя оценка (0)
     * @param rateNum Количество оценок (0)
     */
    protected Book(String bookAuthor, String bookName, boolean status, double rate, int rateNum) {
        this.bookAuthor = bookAuthor;
        this.bookName = bookName;
        this.status = status;
        this.rate = rate;
        this.rateNum = rateNum;
    }
}
