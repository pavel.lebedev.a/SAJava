package homework3_2;

import java.util.ArrayList;

class Library {
    static ArrayList<Book> books  = new ArrayList<Book>();
    static ArrayList<Visitor> visitors  = new ArrayList<Visitor>();
    private static int visitorIdCount = 0; // счётчик регистраций

    // Setters and Getters
    protected ArrayList<Book> getLibraryList() {
        return books;
    }

    protected void setBooks(ArrayList<Book> books) {
        Library.books = books;
    }

    protected ArrayList<Visitor> getVisitorList() {
        return visitors;
    }

    protected void setVisitors(ArrayList<Visitor> visitors) {
        Library.visitors = visitors;
    }

    protected static int getVisitorIdCount() {
        return visitorIdCount;
    }

    protected static void setVisitorIdCount(int visitorIdCount) {
        Library.visitorIdCount = visitorIdCount;
    }

    // Methods

    /**
     * Добавить новую книгу
     * @param bookAuthor Автор
     * @param bookName Название
     */
    protected void addBook(String bookAuthor, String bookName) {
        if (searchBookName(bookName)) { // Проверить что такой книги нет
            System.out.println("Книга с таким названием уже есть");
        } else {
            Book book = new Book(bookAuthor, bookName, true, 0, 0);
            books.add(book); // Добавить книгу в список
            System.out.println("Книга добавлена в список");
        }
    }

    /**
     * Поиск совпадения названия
     * @param bookName Название книги
     * @return true / false
     */
    private boolean searchBookName(String bookName) {
        boolean out = false;
        for (Book book : books) {
            if (book.getBookName().compareTo(bookName) == 0) {
                out = true;
                break;
            }
        }
        return out;
    }

    /**
     * Поиск книги по названию
     * @param bookName Название книги
     * @return Индекс в списке или -1
     */
    private int searchBookN(String bookName) {
        int n = -1;
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getBookName().compareTo(bookName) == 0 && n < 0) {
                n = i;
            }
        }
        return n;
    }

    /**
     * Вывод информации о книге
     * @param n индекс в списке
     */
    private void printBook(int n) {
        if (n >= 0 && n < books.size()) {
            System.out.print(books.get(n).getBookAuthor() + ", " + books.get(n).getBookName() + ", общий рейтинг: " + books.get(n).getRate() + ", на основании " + books.get(n).getRateNum() + " оценок.");
            System.out.println("");
        } else searchError();
    }

    /**
     * Поиск и вывод информации о книге
     * @param bookName Название книги
     */
    protected void searchBook(String bookName) {
        int n = searchBookN(bookName);
        if (n >= 0) {
            printBook(n);
        } else searchError();
    }

    /** Найти и вернуть книгу */
    protected Book searchBookO(String bookName) {
        int n = searchBookN(bookName);
        Book out = new Book();
        if (n >= 0) {
            out = books.get(n);
        } else searchError();
        return out;
    }

    /**
     * Удаляет книгу из списка
     * @param bookName Название книги
     */
    protected void deleteBook(String bookName) {
        int n = searchBookN(bookName);
        if (n >= 0) {
            printBook(n);
            books.remove(n);
            System.out.println("Книга удалена");
        } else searchError();
    }

    /**
     * Сообщение об ошибке в поиске книги
     */
    private void searchError() {
        System.out.println("По вашему запросу ничего не найдено");
    }

    /**
     * Поиск книг по автору
     * @param bookAuthor Автор
     */
    protected void searchAuthor(String bookAuthor) {
        int num = -1;
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getBookAuthor().compareTo(bookAuthor) == 0) {
                num = i;
                printBook(i);
            }
        }
        if (num < 0) System.out.println("Автор " + bookAuthor + " не найден");
    }

    /**
     * Добавить посетителя
     * @param visitorName Имя посетителя
     */
    protected void addVisitor(String visitorName) {
        Visitor visitor = new Visitor(visitorName, "null", true, 0, addVisitorId());
        visitors.add(visitor); // Добавить посетителя в список
    }

    /**
     * Аренда книги
     * @param visitorId Уникальный ID читателя
     * @param bookName Название книги
     */
    protected void bookRental(int visitorId, String bookName) {
        if (visitorId >= 0 && visitorId <= visitors.get(visitors.size() - 1).getVisitorId()) {
            int n = searchBookN(bookName);
            if (n >= 0) { // Она есть в библиотеке.
                int m = searchVisitorId(visitorId); // Найти индекс Пользователя
                if (visitors.get(m).getStatus()) { // У посетителя сейчас нет книги.
                    if (books.get(n).getStatus()) { // Она не одолжена
                        addId(m); // Добавить статус читателя, если ещё нет
                        books.get(n).setStatus(false); // Пометить книгу как одолженную
                        visitors.get(m).setStatus(false); // Пометить посетителя как должника
                        visitors.get(m).setBookCount(visitors.get(m).getBookCount() + 1); // Увеличить счётчик книг
                        printBook(n);
                        System.out.println("Книга выдана посетителю");
                        printVisitor(m);
                    }
                }
            } else searchError();
        } else searchError();
    }

    /**
     * Присвоить id
     */
    private void addId(int visitorId) {
        if (visitors.get(visitorId).getId().compareTo("Читатель") != 0) {
            visitors.get(visitorId).setId("Читатель"); // Присвоить id
        }
    }

    /**
     * Присвоить уникальный id (читательский билет)
     */
    private int addVisitorId() {
        int id = getVisitorIdCount(); // Получить текущий id
        setVisitorIdCount(getVisitorIdCount() + 1); // Увеличить счётчик
        return id;
    }

    /**
     * Поиск пользователя по ID
     * @param visitorId Уникальный ID
     * @return Индекс в списке или -1
     */
    protected int searchVisitorId(int visitorId) {
        int out = -1;
        if (visitorId >= 0) {
            out = recursiveBinarySearch(visitors, visitorId, 0, visitors.size() - 1);
        }  else searchError();
        return out;
    }

    /**
     * поиск и вывод информации о пользователе
     * @param visitorId Уникальный ID
     */
    protected void searchVisitor(int visitorId) {
        if (recursiveBinarySearch(visitors, visitorId, 0, visitors.size() - 1) >= 0) {
            printVisitor(visitorId);
        } else System.out.println("Пользователь с таким ID не найден");
    }

    /**
     * Деление массива и поиск
     *
     * @param visitors Список
     * @param visitorId  Уникальный ID
     * @param low мин
     * @param high макс
     * @return Индекс или -1 если отсутствует искомое
     */
    protected int recursiveBinarySearch(ArrayList<Visitor> visitors, int visitorId, int low, int high) {
        if (low > high) // поиск завершается без единого совпадения
            return -low - 1;

        int mid = (low + high) / 2;
        if (visitorId < visitors.get(mid).getVisitorId())
            return recursiveBinarySearch(visitors, visitorId, low, mid - 1);
        else if (visitorId == visitors.get(mid).getVisitorId())
            return mid;
        else
            return recursiveBinarySearch(visitors, visitorId, mid + 1, high);
    }

    /**
     * Вывод информации о посетителе
     * @param visitorId Уникальный id
     */
    protected void printVisitor(int visitorId) {
        if (visitorId >= 0) {
            System.out.print(visitors.get(visitorId).getVisitorName() + ", UID: " + visitors.get(visitorId).getVisitorId() + ", Status: " + visitors.get(visitorId).getId() +
                    ", Не должник: " + visitors.get(visitorId).getStatus() + ", Брал книг: " + visitors.get(visitorId).getBookCount());
            System.out.println("");
        } else searchError();
    }

    /**
     * Возврат книги
     * @param visitorId Уникальный номер посетителя
     * @param bookName Название книги
     * @param rate Оценка книги
     */
    protected void bookReturn(int visitorId, String bookName, double rate) {
        if (visitorId >= 0 && visitorId <= visitors.get(visitors.size() - 1).getVisitorId()) {
            int n = searchBookN(bookName);
            if (n >= 0) { // Она есть в библиотеке.
                int m = searchVisitorId(visitorId); // Найти индекс Пользователя
                if (visitors.get(m).getStatus()) { // У посетителя сейчас нет книги.
                    System.out.println("У посетителя нет книг");
                } else {
                    if (books.get(n).getStatus()) { // Она не одолжена
                        System.out.println("Книга уже находиться в библиотеке");
                    } else {
                        books.get(n).setStatus(true); // Пометить книгу как свободную
                        visitors.get(m).setStatus(true); // Пометить посетителя как не должника
                        addBookRate(books.get(n), rate);
                        printBook(n);
                        System.out.println("Книга снова в библиотеке");
                    }
                }
            }
        } else searchError();
    }

    /**
     * Оценка книги
     * @param book книга
     * @param rate оценка
     */
    protected void addBookRate (Book book, double rate) {
        double n = ((book.getRate() * book.getRateNum() + rate) / (book.getRateNum() + 1));
        n = (double)Math.round((n * 10)) / 10;
        book.setRate(n);
        book.setRateNum(book.getRateNum() + 1); // Увеличить количество оценок книги
    }

    /** Возвращает оценку книги */
    protected double searchBookRateName (String bookName) {
        return books.get(searchBookN(bookName)).getRate();
    }
}
