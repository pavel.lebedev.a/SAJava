package homework3_2;

class Visitor {
    private String visitorName;
    private String id;
    private int visitorId; // уникальный id каждого посетителя
    private boolean status; // true без книги / false взял книгу
    private int bookCount; // Сколько брал книг

    // Setters and Getters
    protected String getVisitorName() {
        return visitorName;
    }

    protected void setVisitorName(String visitorName) {
        this.visitorName = visitorName;
    }

    protected String getId() {
        return id;
    }

    protected void setId(String id) {
        this.id = id;
    }

    protected boolean getStatus() {
        return status;
    }

    protected void setStatus(boolean status) {
        this.status = status;
    }

    protected int getBookCount() {
        return bookCount;
    }

    protected void setBookCount(int bookNum) {
        this.bookCount = bookNum;
    }

    protected int getVisitorId() {
        return visitorId;
    }

    protected void setVisitorId(int visitorId) {
        this.visitorId = visitorId;
    }


    // Constructors
    /**
     * @param visitorName Имя
     * @param id Идентификатор (0) Пока не взял книгу
     * @param status true без книги / false взял книгу
     * @param bookCount Сколько брал книг (0)
     */
    public Visitor(String visitorName, String id, boolean status, int bookCount, int visitorId) {
        this.visitorName = visitorName;
        this.id = id;
        this.status = status;
        this.bookCount = bookCount;
        this.visitorId = visitorId;
        System.out.println("Посетитель добавлен в список");
    }
}
