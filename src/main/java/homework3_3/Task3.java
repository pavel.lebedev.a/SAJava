package homework3_3;
/*
На вход передается N — количество столбцов в двумерном массиве и M —
количество строк. Необходимо вывести матрицу на экран, каждый элемент
которого состоит из суммы индекса столбца и строки этого же элемента. Решить
необходимо используя ArrayList.
 */
import java.util.ArrayList;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt(); // количество столбцов в двумерном массиве
        int m = scanner.nextInt(); // количество строк в двумерном массиве
        ArrayList<ArrayList<Integer>> matrixList = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < m; i++) { // заполнение матрицы
            ArrayList<Integer> List = new ArrayList<>();
            for (int j = 0; j < n; j++) { // строками
                List.add(i + j); // с суммой
            }
            matrixList.add(List);
        }
        for (int i = 0; i < m; i++) { // Печать матрицы
            System.out.println(matrixList.get(i).toString()
                    .replace(",", "")  //без запятых
                    .replace("[", "")  //и
                    .replace("]", "")  //скобок
            );
        }
    }
}
