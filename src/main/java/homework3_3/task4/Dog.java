package homework3_3.task4;

public class Dog {
    String name; // Имя собаки
    Participant participant; // Ссылка на владельца
    double grade; // Средняя оценка на конкурсе

    /**
     * Конструктор. Имя собаки, Ссылка на владельца, Массив оценок на конкурсе
     */
    public Dog(String name, Participant participant, double grade) {
        this.name = name;
        this.participant = participant;
        this.grade = grade;
    }

    public String getName() {
        return name;
    }


    public Participant getParticipant() {
        return participant;
    }

    public double getGrade() {
        return grade;
    }
}
