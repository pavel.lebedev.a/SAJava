package homework3_3.task4;

import java.util.Scanner;

public class Competition {
    static Dog[] dogs;

    public Competition() {
    }


    public Dog[] getDogs() {
        return dogs;
    }

    /**
     * Инициализация ввода данных и создание объектов
     */
    public void input() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество участников");
        int n = scanner.nextInt(); // количество участников
        System.out.println("Введите имена владельцев");
        Participant[] participants = new Participant[n]; // Массив владельцев
        for (int i = 0; i < n; i++) {
            participants[i] = new Participant(scanner.next());
        }
        System.out.println("Введите имена собак");
        String[] dogName = new String[n]; // Имена собак
        for (int i = 0; i < dogName.length; i++) { // Заполнение массива
            dogName[i] = scanner.next();
        }
        double[] grades = finalGrade(n);
        dogs = new Dog[n]; // Список собак
        for (int i = 0; i < n; i++) {
            dogs[i] = new Dog(dogName[i], participants[i], grades[i]);
        }
    }

    /**
     * Получение матрицы оценок и средней оценки
     */
    private double[] finalGrade(int n) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите массив оценок");
        double[][] grades = new double[n][3]; // Массив оценок
        for (int i = 0; i < grades.length; i++) { // Заполнение матрицы с консоли
            for (int j = 0; j < grades[i].length; j++) {
                grades[i][j] = scanner.nextInt();
            }
        }
        double[] finalGrades = new double[n]; // Массив средней оценки
        for (int i = 0; i < grades.length; i++) {
            double sum = 0.0;
            for (int j = 0; j < grades[i].length; j++) { // подсчёт суммы оценок
                sum += grades[i][j];
            }
            finalGrades[i] = (double) ((int) ((sum / 3) * 10)) / 10; // Запись средней оценки
        }
        return finalGrades;
    }

    /**
     * Вывод информации обо всех участниках
     */
    public void printAllInfo() { // Имя хозяина: кличка, средняя оценка
        for (Dog dog : dogs) {
            System.out.println(dog.getParticipant().getName() + " " + dog.getName() + " " + dog.getGrade());
        }
    }

    /**
     * Вывод информации о 3 лучших
     */
    public void printBestDogs() {
        sortDogs();
        for (int i = 0; i < dogs.length && i < 3; i++) {
            System.out.println(dogs[i].getParticipant().getName() + " " + dogs[i].getName() + " " + dogs[i].getGrade());
        }
    }

    /**
     * Сортировка массива собак пузырьком
     */
    private void sortDogs() {
        for (int i = 0; i < dogs.length; i++) {
            for (int j = 1; j < dogs.length - i; j++) {
                if (dogs[j - 1].getGrade() < dogs[j].getGrade()) {
                    Dog temp = dogs[j - 1];
                    dogs[j - 1] = dogs[j];
                    dogs[j] = temp;
                }
            }
        }
    }
}
