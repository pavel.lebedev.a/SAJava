package homework3_3.task4;

public class Test {
    public static void main(String[] args) {
        Competition test = new Competition();
        test.input();
        test.printAllInfo();
        System.out.println("\nПобедители");
        test.printBestDogs();
    }
}
/*
Входные данные
4
Иван
Николай
Анна
Дарья
Жучка
Кнопка
Цезарь
Добряш
7 6 7
8 8 7
4 5 6
9 9 9
 */