package homework3_3.task4;

public class Participant {
    String name; // Имя владельца

    /**
     * Конструктор. Имя владельца
     */
    public Participant(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}