package homework3_3.task2;
/*
Цех по ремонту BestCarpenterEver умеет чинить некоторую Мебель. К
сожалению, из Мебели он умеет чинить только Табуретки, а Столы, например,
нет. Реализовать метод в цеху, позволяющий по переданной мебели
определять, сможет ли ей починить или нет. Возвращать результат типа
boolean. Протестировать метод.
Задача легкая, усложнять не стоит, направлена на использование instanceof
 */
public class Test {
    public static void main(String[] args) {
        Table table = new Table(); // стол
        Stool stool = new Stool(); // табурет
        BestCarpenterEver test = new BestCarpenterEver(); // сервис
        System.out.println("Test1. Чиним стол?");
        System.out.println(test.repair(table));
        test.repair2(table);
        System.out.println("Test1. Чиним табурет?");
        System.out.println(test.repair(stool));
        test.repair2(stool);
    }
}
