package homework3_3.task1;

public class Bat extends Mammal {
    @Override
    public void name() {
        System.out.println("\nBat");
    }

    @Override
    public void sleep() {
        super.sleep();
    }

    @Override
    public void eat() {
        super.eat();
    }

    @Override
    public void movement() {
        System.out.println("Летает медленно.");
    }

    @Override
    public void wayOfBirth() {
        super.wayOfBirth();
    }
}
