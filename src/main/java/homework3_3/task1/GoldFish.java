package homework3_3.task1;

public class GoldFish extends Fish {
    @Override
    public void name() {
        System.out.println("\nGoldFish");
    }

    @Override
    public void sleep() {
        super.sleep();
    }

    @Override
    public void eat() {
        super.eat();
    }

    @Override
    public void movement() {
        System.out.println("Плавает медленно.");
    }

    @Override
    public void wayOfBirth() {
        super.wayOfBirth();
    }
}
