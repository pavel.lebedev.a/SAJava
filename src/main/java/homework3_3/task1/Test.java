package homework3_3.task1;

public class Test {
    public static void main(String[] args) {
        Bat bat = new Bat();
        bat.name();
        bat.eat();
        bat.sleep();
        bat.movement();
        bat.wayOfBirth();

        Dolphin dolphin = new Dolphin();
        dolphin.name();
        dolphin.eat();
        dolphin.sleep();
        dolphin.movement();
        dolphin.wayOfBirth();

        GoldFish goldFish = new GoldFish();
        goldFish.name();
        goldFish.eat();
        goldFish.sleep();
        goldFish.movement();
        goldFish.wayOfBirth();

        Eagle eagle = new Eagle();
        eagle.name();
        eagle.eat();
        eagle.sleep();
        eagle.movement();
        eagle.wayOfBirth();
    }
}
