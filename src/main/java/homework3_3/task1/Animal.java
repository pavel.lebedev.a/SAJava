package homework3_3.task1;

public abstract class Animal {
    public void name () {}
    public void sleep () {
        System.out.println("Спит.");
    }
    public void eat () {
        System.out.println("Ест.");
    }
    public void movement () {}
    public void wayOfBirth () {}
}
