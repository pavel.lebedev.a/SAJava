package homework3_3.task1;

public class Dolphin extends Mammal {
    @Override
    public void name() {
        System.out.println("\nDolphin");
    }

    @Override
    public void sleep() {
        super.sleep();
    }

    @Override
    public void eat() {
        super.eat();
    }

    @Override
    public void movement() {
        System.out.println("Плавает быстро.");
    }

    @Override
    public void wayOfBirth() {
        super.wayOfBirth();
    }
}
