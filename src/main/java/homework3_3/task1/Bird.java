package homework3_3.task1;

public class Bird extends Animal {
    @Override
    public void name() {
        super.name();
    }

    @Override
    public void sleep() {
        super.sleep();
    }

    @Override
    public void eat() {
        super.eat();
    }

    @Override
    public void movement() {
        super.movement();
    }

    @Override
    public void wayOfBirth() {
        System.out.println("Яйцекладущее.");
    }
}
