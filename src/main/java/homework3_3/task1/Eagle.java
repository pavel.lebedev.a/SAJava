package homework3_3.task1;

public class Eagle extends Bird {
    @Override
    public void name() {
        System.out.println("\nEagle");
    }

    @Override
    public void sleep() {
        super.sleep();
    }

    @Override
    public void eat() {
        super.eat();
    }

    @Override
    public void movement() {
        System.out.println("Летает быстро.");
    }

    @Override
    public void wayOfBirth() {
        super.wayOfBirth();
    }
}
