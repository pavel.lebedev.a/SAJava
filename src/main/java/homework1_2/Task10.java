package homework1_2;
/*
"А логарифмическое?" - не унималась дочь.
Напишите программу, которая проверяет, что log(e^n) == n для любого
вещественного n.
Ограничения:
-500 < n < 500
Пример:
Входные данные Выходные данные
1,0 true
12,34 true
-42,10 true
 */

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double n = input.nextDouble();
        double b = Math.log(Math.pow(Math.E, n));
        if (b == n) System.out.println("true");
        else System.out.println("false");
    }
}
