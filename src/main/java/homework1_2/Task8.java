package homework1_2;
/*
Раз так легко получается разделять по первому пробелу, Петя решил
немного изменить предыдущую программу и теперь разделять строку по
последнему пробелу.
Ограничения:
В строке гарантированно есть хотя бы один пробелJava 12 Базовый модуль Неделя 2
ДЗ 1 Часть 2
Первый и последний символ строки гарантированно не пробел
2 < s.length() < 100
Пример:
Входные данные Выходные данные
Hi great team! Hi great
team!
Hello world! Hello
world!
*/

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String message = input.nextLine();

        int k = message.lastIndexOf(' '); // находим Последний пробел

        System.out.println(message.substring(0, k)); // вывод до пробела
        System.out.println(message.substring(k + 1)); // вывод после пробела
    }
}
