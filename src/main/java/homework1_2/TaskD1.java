package homework1_2;
/*
У Марата был взломан пароль. Он решил написать программу,
которая проверяет его пароль на сложность. В интернете он узнал, что пароль
должен отвечать следующим требованиям:
● пароль должен состоять из хотя бы 8 символов;Java 12 Базовый модуль Неделя 2
ДЗ 1 Часть 2
● в пароле должны быть:
○ заглавные буквы
○ строчные символы
○ числа
○ специальные знаки(_*-)
Если пароль прошел проверку, то программа должна вывести в консоль строку пароль
надежный, иначе строку: пароль не прошел проверку
Входные данные Выходные данные
Hello_22 пароль надежный
world234 пароль не прошел проверку
 */

import java.util.Scanner;

public class TaskD1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String password = input.nextLine();
        int passLine = password.length(); // длинна строки
        if (passLine >= 8) { // больше 8 символов
            int lowCase, upCase, numbers, specSymbols; // переменные для условий
            lowCase = upCase = numbers = specSymbols = 0;
            char n; // переменная для символа
            for (int i = 0; i < passLine; i++) {
                n = password.charAt(i); // присвоение символа по индексу
                if (n >= 'A' && n <= 'Z') upCase = 1; // Заглавные буквы есть
                if (n >= 'a' && n <= 'z') lowCase = 1; // Строчные буквы есть
                if (n >= '0' && n <= '9') numbers = 1; // Числа есть
                if (n == '_' || n == '*' || n == '-') specSymbols = 1; // Спецзнаки есть
            }
            if (lowCase + upCase + numbers + specSymbols == 4) System.out.println("пароль надежный");
            else System.out.println("пароль не прошел проверку");
        } else System.out.println("пароль не прошел проверку");
    }
}
