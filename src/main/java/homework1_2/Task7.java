package homework1_2;
/*
Петя недавно изучил строки в джаве и решил попрактиковаться с ними.
Ему хочется уметь разделять строку по первому пробелу. Для этого он может
воспользоваться методами indexOf() и substring().
На вход подается строка. Нужно вывести две строки, полученные из входной
разделением по первому пробелу.
Ограничения:
В строке гарантированно есть хотя бы один пробел
Первый и последний символ строки гарантированно не пробел
2 < s.length() < 100
Пример:
Входные данные Выходные данные
Hi great team!
Hi
great team!
Hello world!
Hello
world!
 */

import java.util.Scanner;

public class Task7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String message = input.nextLine();

        int k = message.indexOf(' '); // находим первый пробел

        System.out.println(message.substring(0, k)); // вывод до пробела
        System.out.println(message.substring(k + 1)); // вывод после пробела
    }
}
