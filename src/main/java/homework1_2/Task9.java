package homework1_2;
/*
Пока Петя практиковался в работе со строками, к нему подбежала его
дочь и спросила: "А правда ли, что тригонометрическое тождество (sin^2(x)+
cos^2(x) - 1 == 0) всегда-всегда выполняется?"
Напишите программу, которая проверяет, что при любом x на входе
тригонометрическое тождество будет выполняться (то есть будет выводить true
при любом x).
Ограничения:
-1000 < x < 1000
Пример:
Входные данные Выходные данные
90 true
0 true
-200 true
 */

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        double b = Math.pow(Math.sin(a), 2) + Math.pow(Math.cos(a), 2) - 1;
        if ((int) b == 0) System.out.println("true");
        else System.out.println("false");
    }
}
