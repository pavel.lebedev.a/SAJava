package homework1_2;
/*
Петя пришел домой и помогает дочке решать математику. Ей нужно
определить, принадлежит ли точка с указанными координатами первому
квадранту. Недолго думая, Петя решил автоматизировать процесс и написать
программу: на вход нужно принимать два целых числа (координаты точки),
выводить true, когда точка попала в квадрант и false иначе.
Но сначала Петя вспомнил, что точка лежит в первом квадранте тогда, когда её
координаты удовлетворяют условию: x > 0 и y > 0.
Ограничения:
-100 < x, y < 100
Пример:
Входные данные Выходные данные
-50 -12 false
42 15 true
0 23 false
46 26 true
*/

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int b = input.nextInt();

        if (a > 0 && b > 0) {
            System.out.println("true");
        } else System.out.println("false");
    }
}
