package homework1_2;

/*
Старый телефон Андрея сломался, поэтому он решил приобрести
новый. Продавец телефонов предлагает разные варианты, но Андрея
интересуют только модели серии samsung или iphone. Также Андрей решил
рассматривать телефоны только от 50000 до 120000 рублей. Чтобы не тратить
время на разговоры, Андрей хочет написать программу, которая поможет ему
сделать выбор.
На вход подается строка – модель телефона и число – стоимость телефона.
Нужно вывести "Можно купить", если модель содержит слово samsung или
iphone и стоимость от 50000 до 120000 рублей включительно. Иначе вывести
"Не подходит".
Гарантируется, что в модели телефона не указано одновременно несколько
серий.
Входные данные / Выходные данные
iphone XL 58000 Можно купить
nokia 10 100000 Не подходит
samsung Galaxy (GT-I7500) 40000 Не подходит
samsung S 21 120000 Можно купить
 */

import java.util.Scanner;

public class TaskD3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String modelPhone = input.nextLine();
        int coastPhone = input.nextInt();
        if (coastPhone >= 50000 && coastPhone <= 120000) {
            if (modelPhone.contains("samsung") || modelPhone.contains("iphone")) {
                System.out.println("Можно купить");
            } else System.out.println("Не подходит");
        } else System.out.println("Не подходит");
    }
}
