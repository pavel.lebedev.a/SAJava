package homework1_2;
/*
Петя снова пошел на работу. С сегодняшнего дня он решил ходить на
обед строго после полудня. Периодически он посматривает на часы (x - час,
который он увидел). Помогите Пете решить, пора ли ему на обед или нет. Если
время больше полудня, то вывести "Пора". Иначе - “Рано”.
Ограничения:
0 <= n <= 23
Пример:
Входные данные Выходные данные
7 Рано
13 Пора
12 Рано
22 Пора
*/

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();

        if (a >= 13 && a <= 23) {
            System.out.println("Пора");
        } else System.out.println("Рано");
    }
}
